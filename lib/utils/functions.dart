import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:geolocator/geolocator.dart';

String formattedDate(String dateString) {
  final DateFormat formatter = DateFormat('dd.MM.yyyy HH:mm');
  final date = DateTime.parse(dateString.substring(0, 19));
  return formatter.format(date);
}

String dateForCreateAdv() {
  final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
  final date = DateTime.now();
  return formatter.format(date) + ' +0900';
}

String dateforAdv() {
  final DateFormat formatter = DateFormat('yy-MM-dd-HH-mm');
  final date = DateTime.now();
  return formatter.format(date);
}

Future<Position?> requestLocationPermission() async {
  // final serviceStatusLocation = await Permission.locationWhenInUse.isGranted;

  // bool isLocation = serviceStatusLocation == ServiceStatus.enabled;

  final status = await Permission.locationWhenInUse.request();

  if (status == PermissionStatus.granted) {
    // ignore: avoid_print
    print('Permission Granted');
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    return position;
  } else if (status == PermissionStatus.denied) {
    // ignore: avoid_print
    print('Permission denied');
  } else if (status == PermissionStatus.permanentlyDenied) {
    // ignore: avoid_print
    print('Permission Permanently Denied');
    await openAppSettings();
  }
  return null;
}

Future pickImage(ImageSource source) async {
  final ImagePicker _picker = ImagePicker();
  final pickedFile = await _picker.pickImage(source: source, imageQuality: 50);
  if (pickedFile == null) {
    return;
  }

  var file = await ImageCropper().cropImage(
      sourcePath: pickedFile.path,
      aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1));
  if (file == null) {
    return;
  }

  // file = await compressImage(file.path, 35);

  await uploadFile(file.path);
}

Future uploadFile(String path) async {
  final ref = FirebaseStorage.instance
      .ref()
      .child('avatars')
      .child(FirebaseAuth.instance.currentUser!.uid + '.jpg');

  final result = await ref.putFile(File(path));
  final fileUrl = await result.ref.getDownloadURL();
  await FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .update({'imageUrl': fileUrl});

  // setState(() {
  //   imageUrl = fileUrl;
  // });

  // widget.onFileChanged(fileUrl);
}

Future selectPhoto(BuildContext context) async {
  await showModalBottomSheet(
    useRootNavigator: false,
    context: context,
    builder: (context) => BottomSheet(
      builder: (context) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            leading: const Icon(Icons.camera),
            title: const Text('Камера'),
            onTap: () {
              Navigator.of(context).pop();
              pickImage(ImageSource.camera);
            },
          ),
          ListTile(
            leading: const Icon(Icons.filter),
            title: const Text('Открыть Галерею'),
            onTap: () {
              Navigator.of(context).pop();
              pickImage(ImageSource.gallery);
            },
          ),
          const SizedBox(
            height: 50,
          ),
        ],
      ),
      onClosing: () {},
    ),
  );
}

Future updateLastLogin() async {
  final date = dateForCreateAdv();
  FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .update({'last_login': date});
}

Future saveImageToFirebaseStorageFromUrl(Uri imgUrl) async {
  var response = await http.get(imgUrl);
  Directory documentDirectory = await getApplicationDocumentsDirectory();
  File file = File(join(documentDirectory.path, 'template.png'));
  file.writeAsBytesSync(response.bodyBytes);
  final ref = FirebaseStorage.instance
      .ref()
      .child('avatars')
      .child(FirebaseAuth.instance.currentUser!.uid + '.jpg');
  await ref.putFile(file);
}

// Future<Position> determinePosition() async {
//   bool serviceEnabled;
//   LocationPermission permission;

//   // Test if location services are enabled.
//   serviceEnabled = await Geolocator.isLocationServiceEnabled();
//   if (!serviceEnabled) {
//     // Location services are not enabled don't continue
//     // accessing the position and request users of the 
//     // App to enable the location services.
//     return Future.error('Location services are disabled.');
//   }

//   permission = await Geolocator.checkPermission();
//   if (permission == LocationPermission.denied) {
//     permission = await Geolocator.requestPermission();
//     if (permission == LocationPermission.denied) {
//       // Permissions are denied, next time you could try
//       // requesting permissions again (this is also where
//       // Android's shouldShowRequestPermissionRationale 
//       // returned true. According to Android guidelines
//       // your App should show an explanatory UI now.
//       return Future.error('Location permissions are denied');
//     }
//   }
  
//   if (permission == LocationPermission.deniedForever) {
//     // Permissions are denied forever, handle appropriately. 
//     return Future.error(
//       'Location permissions are permanently denied, we cannot request permissions.');
//   } 

//   // When we reach here, permissions are granted and we can
//   // continue accessing the position of the device.
//   return await Geolocator.getCurrentPosition();
// }