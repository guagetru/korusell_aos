import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CarouselAdv extends StatefulWidget {
  final String images;
  final String uid;
  double currentIndexPage;
  CarouselAdv(this.images, this.uid, this.currentIndexPage, {Key? key})
      : super(key: key);

  @override
  State<CarouselAdv> createState() => _CarouselAdvState();
}

class _CarouselAdvState extends State<CarouselAdv> {
  @override
  Widget build(BuildContext context) {
    Future<List<String>> getLinks() async {
      List<String> links = [];
      for (var i = 0; i < int.parse(widget.images); i++) {
        Reference db = FirebaseStorage.instance
            .ref('advImages/${widget.uid + 'ADV' + i.toString() + '.jpg'}');
        links.add(await db.getDownloadURL());
      }
      return links;
    }

    return FutureBuilder<List<String>>(
        future: getLinks(),
        builder: (BuildContext context, snapshot) {
          return Column(
            children: [
              Center(
                child: CarouselSlider.builder(
                  itemCount: int.parse(widget.images),
                  itemBuilder:
                      (BuildContext context, int itemIndex, int pageViewIndex) {
                    if (!snapshot.hasData) {
                      return const SizedBox();
                      // return const Center(child: CircularProgressIndicator());
                    }
                    if (snapshot.hasData) {
                      // print(snapshot.data);
                      List<String> links = snapshot.data as List<String>;
                      return Image.network(links[itemIndex], errorBuilder:
                          (BuildContext context, Object exception,
                              StackTrace? stackTrace) {
                        return const SizedBox();
                        // const Image( image: AssetImage('assets/images/splash.png'));
                      });
                    }
                    return const SizedBox();
                  },
                  options: CarouselOptions(
                    onPageChanged: ((index, reason) => setState(() {
                          widget.currentIndexPage = index.ceilToDouble();
                          // print(widget.currentIndexPage);
                        })),
                    enableInfiniteScroll: false,
                    autoPlay: false,
                    enlargeCenterPage: true,
                    viewportFraction: 1,
                    aspectRatio: 1,
                    initialPage: 0,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              int.parse(widget.images) > 1
                  ? Center(
                      child: DotsIndicator(
                        decorator: const DotsDecorator(
                          activeColor: Colors.blue,
                          // activeSize: Size.square(10),
                          size: Size.square(7),
                          spacing: EdgeInsets.symmetric(horizontal: 4),
                        ),
                        mainAxisSize: MainAxisSize.min,
                        dotsCount: int.parse(widget.images),
                        position: widget.currentIndexPage,
                      ),
                    )
                  : const SizedBox(),
            ],
          );
        });
  }
}
