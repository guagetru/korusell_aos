import 'package:flutter/material.dart';
import 'package:firebase_image/firebase_image.dart';

class CustomImage extends StatelessWidget {
  final String imageUrl;
  final double height;
  final double width;
  final bool cache;
  const CustomImage(this.imageUrl, this.height, this.width, this.cache,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image(
      image: FirebaseImage(imageUrl,
          shouldCache: cache, // The image should be cached (default:  True)
          maxSizeBytes: 2000 * 1000, // 3MB max file size (default: 2.5MB)
          cacheRefreshStrategy: CacheRefreshStrategy
              .BY_METADATA_DATE, // Switch off update checking
          scale: 1),
      height: height,
      width: width,
      errorBuilder:
          (BuildContext context, Object exception, StackTrace? stackTrace) {
        return Image(
          image: const AssetImage('assets/images/splash.png'),
          height: height,
          width: width,
        );
      },
    );
  }
}
