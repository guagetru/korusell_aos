import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'package:work_app/utils/image_controller/appround_image.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:firebase_storage/firebase_storage.dart';

class UserImage extends StatefulWidget {
  final Function(String imageUrl) onFileChanged;
  final String imageUrl;
  const UserImage({Key? key, required this.onFileChanged, this.imageUrl = ''})
      : super(key: key);

  @override
  _UserImageState createState() => _UserImageState();
}

class _UserImageState extends State<UserImage> {
  final ImagePicker _picker = ImagePicker();
  late String imageUrl;

  @override
  void initState() {
    super.initState();

    imageUrl = widget.imageUrl;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (imageUrl == '')
          const Icon(
            CupertinoIcons.profile_circled,
            size: 80,
          ),
        if (imageUrl != '')
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () => _selectPhoto(),
            child: AppRoundImage.url(imageUrl, height: 80, width: 80),
          ),
        InkWell(
          onTap: () => _selectPhoto(),
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              widget.imageUrl != '' ? 'Изменить фото' : 'Выбрать фото',
              style: const TextStyle(
                color: Colors.blue,
              ),
            ),
          ),
        )
      ],
    );
  }

  Future _uploadFile(String path) async {
    final ref = FirebaseStorage.instance
        .ref()
        .child('avatars')
        .child(FirebaseAuth.instance.currentUser!.uid + '.jpg');
    // .child(DateTime.now().toIso8601String() + p.basename(path));

    final result = await ref.putFile(File(path));
    final fileUrl = await result.ref.getDownloadURL();
    await FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({'imageUrl': fileUrl});
    // .set({'imageUrl': fileUrl});
    setState(() {
      imageUrl = fileUrl;
    });

    widget.onFileChanged(fileUrl);
  }

  Future<File?> compressImage(String path, int quality) async {
    final newPath = p.join((await getTemporaryDirectory()).path,
        '${DateTime.now()}.${p.extension(path)}');

    final result = await FlutterImageCompress.compressAndGetFile(
      path,
      newPath,
      quality: quality,
    );

    return result;
  }

  Future _pickImage(ImageSource source) async {
    final pickedFile =
        await _picker.pickImage(source: source, imageQuality: 50);
    if (pickedFile == null) {
      return;
    }

    var file = await ImageCropper().cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1));
    if (file == null) {
      return;
    }

    file = await compressImage(file.path, 35);

    await _uploadFile(file!.path);
  }

  Future _selectPhoto() async {
    await showModalBottomSheet(
      useRootNavigator: false,
      context: context,
      builder: (context) => BottomSheet(
        builder: (context) => Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: const Icon(Icons.camera),
              title: const Text('Камера'),
              onTap: () {
                Navigator.of(context).pop();
                _pickImage(ImageSource.camera);
              },
            ),
            ListTile(
              leading: const Icon(Icons.filter),
              title: const Text('Открыть Галерею'),
              onTap: () {
                Navigator.of(context).pop();
                _pickImage(ImageSource.gallery);
              },
            ),
            const SizedBox(
              height: 70,
            ),
          ],
        ),
        onClosing: () {},
      ),
    );
  }
}
