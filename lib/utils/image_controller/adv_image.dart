import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:work_app/utils/constants.dart';

import 'carousel_adv.dart';

class AdvImage extends StatefulWidget {
  final int images;
  final String uid;
  const AdvImage(this.images, this.uid, {Key? key}) : super(key: key);

  @override
  State<AdvImage> createState() => _AdvImageState();
}

class _AdvImageState extends State<AdvImage> {
  String? singleImage;
  List<String> multipleImages = [];
  // String uid = FirebaseAuth.instance.currentUser!.uid;
  double currentIndexPage = 0.0;
  List<String> imageNames = [];

  Widget carousel(List<String> path) {
    return path.isNotEmpty
        ? Column(
            children: [
              Center(
                child: CarouselSlider.builder(
                  itemCount: path.length,
                  itemBuilder:
                      (BuildContext context, int itemIndex, int pageViewIndex) {
                    // print(path[itemIndex]);
                    return Image.network(path[itemIndex],
                        width: double.infinity, loadingBuilder:
                            (BuildContext context, Widget child,
                                ImageChunkEvent? loadingProgress) {
                      if (loadingProgress == null) {
                        return child;
                      }
                      return Center(
                        child: CircularProgressIndicator(
                          value: loadingProgress.expectedTotalBytes != null
                              ? loadingProgress.cumulativeBytesLoaded /
                                  loadingProgress.expectedTotalBytes!
                              : null,
                        ),
                      );
                    });
                  },
                  options: CarouselOptions(
                    onPageChanged: ((index, reason) => setState(() {
                          currentIndexPage = index.ceilToDouble();
                          // print(currentIndexPage);
                        })),
                    enableInfiniteScroll: false,
                    autoPlay: false,
                    enlargeCenterPage: true,
                    viewportFraction: 1,
                    aspectRatio: 1,
                    initialPage: 0,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Center(
                child: DotsIndicator(
                  decorator: const DotsDecorator(
                    activeColor: Colors.blue,
                    // activeSize: Size.square(10),
                    size: Size.square(7),
                    spacing: EdgeInsets.symmetric(horizontal: 4),
                  ),
                  mainAxisSize: MainAxisSize.min,
                  dotsCount: path.length,
                  position: currentIndexPage,
                ),
              ),
            ],
          )
        : widget.images > 0
            ? CarouselAdv(
                widget.images.toString(), widget.uid, currentIndexPage)
            : const SizedBox();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        // height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            carousel(multipleImages),
            TextButton(
              onPressed: () async {
                List<XFile> _images = await multiImagePicker();
                if (_images.isNotEmpty) {
                  multipleImages = await multiImageUploader(_images);
                  // print(multipleImages);
                  setState(() {
                    advImagesCount = multipleImages.length;
                    advImagesLinks = multipleImages;
                    // print('QQQQQ ${advImagesLinks}');
                  });
                }
              },
              child: const Text(
                'Выбрать изображения',
                // style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            const Text('(максимум 4 изображения)')
          ],
        ));
  }

  Future<List<String>> multiImageUploader(List<XFile> list) async {
    List<String> _path = [];
    var counter = 0;
    for (XFile _image in list) {
      // _path.add(await uploadImage(_image));
      _path.add(await uploadImage(_image, counter));
      // _path.add(uid + counter.toString() + '.jpg');
      counter++;
    }
    return _path;
  }

  Future<XFile?> singleImagePick() async {
    return await ImagePicker().pickImage(source: ImageSource.gallery);
  }

  Future<List<XFile>> multiImagePicker() async {
    List<XFile>? _images = await ImagePicker().pickMultiImage(imageQuality: 20);
    if (_images != null && _images.isNotEmpty && _images.length < 5) {
      return _images;
    }
    return [];
  }

  Future<String> uploadImage(XFile image, int count) async {
    // print(getImageName(image));

    Reference db = FirebaseStorage.instance
        .ref('advImages/${widget.uid + 'ADV' + count.toString() + '.jpg'}');
    await db.putFile(File(image.path));

    return await db.getDownloadURL();
  }
}

String getImageName(XFile image) {
  return image.path.split('/').last;
}
