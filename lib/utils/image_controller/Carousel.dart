// ignore_for_file: file_names

import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Carousel extends StatefulWidget {
  final String images;
  double currentIndexPage;
  Carousel(this.images, this.currentIndexPage, {Key? key}) : super(key: key);

  @override
  State<Carousel> createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  @override
  Widget build(BuildContext context) {
    Future<List<String>> getLinks() async {
      List<String> links = [];
      for (var i = 0; i < int.parse(widget.images); i++) {
        Reference db = FirebaseStorage.instance.ref(
            'images/${FirebaseAuth.instance.currentUser!.uid + i.toString() + '.jpg'}');
        links.add(await db.getDownloadURL());
      }
      return links;
    }

    return FutureBuilder<List<String>>(
        future: getLinks(),
        builder: (BuildContext context, snapshot) {
          return Column(
            children: [
              Center(
                child: CarouselSlider.builder(
                  itemCount: int.parse(widget.images),
                  itemBuilder:
                      (BuildContext context, int itemIndex, int pageViewIndex) {
                    if (!snapshot.hasData) {
                      return const SizedBox();
                      // return const Center(child: CircularProgressIndicator());
                    }
                    if (snapshot.hasData) {
                      // print(snapshot.data);
                      List<String> links = snapshot.data as List<String>;
                      return Image.network(links[itemIndex], errorBuilder:
                          (BuildContext context, Object exception,
                              StackTrace? stackTrace) {
                        return const SizedBox();
                        // const Image( image: AssetImage('assets/images/splash.png'));
                      });
                    }
                    return const SizedBox();
                  },
                  options: CarouselOptions(
                    onPageChanged: ((index, reason) => setState(() {
                          widget.currentIndexPage = index.ceilToDouble();
                          // print(widget.currentIndexPage);
                        })),
                    enableInfiniteScroll: false,
                    autoPlay: false,
                    enlargeCenterPage: true,
                    viewportFraction: 1,
                    aspectRatio: 1,
                    initialPage: 0,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Center(
                child: DotsIndicator(
                  decorator: const DotsDecorator(
                    activeColor: Colors.blue,
                    // activeSize: Size.square(10),
                    size: Size.square(7),
                    spacing: EdgeInsets.symmetric(horizontal: 4),
                  ),
                  mainAxisSize: MainAxisSize.min,
                  dotsCount: int.parse(widget.images),
                  position: widget.currentIndexPage,
                ),
              ),
            ],
          );
        });
  }
}
