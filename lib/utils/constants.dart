import 'package:firebase_core/firebase_core.dart';

const firebaseOptions = FirebaseOptions(
    apiKey: 'AIzaSyCU-hKKKLe19ldnASMbZMNcF-WXZEVI3eg',
    appId: '1:558901653552:android:847a529cb5e74f28b47df5',
    messagingSenderId: '558901653552',
    projectId: 'korusell',
    databaseURL:
        'https://korusell-default-rtdb.asia-southeast1.firebasedatabase.app/',
    storageBucket: 'korusell.appspot.com');

const List<String> cities = [
  "Все Города",
  "Ансан",
  "Хвасонг",
  "Инчхон",
  "Сеул",
  "Сувон",
  "Асан",
  "Чхонан",
  "Чхонджу",
  "Пхёнтхэк",
  "Сосан",
  "Дунпо",
  "Другой Город"
];

const List<String> citiesN = [
  "Ансан",
  "Хвасонг",
  "Инчхон",
  "Сеул",
  "Сувон",
  "Асан",
  "Чхонан",
  "Чхонджу",
  "Пхёнтхэк",
  "Сосан",
  "Дунпо",
  "Другой Город"
];

// ignore: constant_identifier_names
const List<String> DBcities = [
  "Ансан",
  "Хвасонг",
  "Инчхон",
  "Сеул",
  "Сувон",
  "Асан",
  "Чхонан",
  "Чхонджу",
  "Пхёнтхэк",
  "Сосан",
  "Дунпо",
];

const categoriesNames = [
  "Все",
  "Завод",
  "Стройка",
  "Мотель",
  "Общепит",
  "Сельхоз работы",
  "Почта/Доставка",
  "Работа в офисе",
  "Другая работа"
];

const List<String> categories = [
  "all",
  "factory",
  "construction",
  "motel",
  "cafe",
  "farm",
  "delivery",
  "office",
  "otherwork"
];

const List<String> subcategories = [
  "factory",
  "construction",
  "motel",
  "cafe",
  "farm",
  "delivery",
  "office",
  "otherwork"
];

const workIcons = {
  "factory": ["🏭", "Завод", "Завод"],
  "construction": ["👷🏻‍♀️", "Стройка", "Стройка"],
  "motel": ["🏩", "Мотель", "Мотель"],
  "cafe": ["🍽", "Общепит", "Общепит"],
  "farm": ["🧑🏽‍🌾", "Сельхоз работы", "Сельхоз"],
  "delivery": ["📦", "Почта/Доставка", "Почта"],
  "office": ["💼", "Работа в офисе", "Офис"],
  "otherwork": ["👨‍🚀", "Другая работа", "Другая"],
};

// ignore: constant_identifier_names
const List<String> DBcategories = [
  "all",
  "transport",
  "house",
  "phone",
  "hobby",
  "car",
  "electronic",
  "children",
  "clothes",
  // "work",
  "sport",
  "change",
  "other",
];

const List<String> advCategoriesN = [
  "transport",
  "house",
  "phone",
  "hobby",
  "car",
  "electronic",
  "children",
  "clothes",
  "sport",
  "change",
  "other",
];

const advNamesN = {
  "transport": "Транспорт",
  "house": "Недвижимость",
  "phone": "Телефоны и Аксессуары",
  "hobby": "Для дома, хобби",
  "car": "Автозапчасти а Аксессуары",
  "electronic": "Электроника",
  "children": "Детские товары",
  "clothes": "Одежда",
  "work": "Домашние животные",
  "sport": "Спорт, туризм и отдых",
  "change": "Обмен, отдам бесплатно",
  "other": "Другое",
};

const advNames = {
  "all": "Все",
  "transport": "Транспорт",
  "house": "Недвижимость",
  "phone": "Телефоны и Аксессуары",
  "hobby": "Для дома, хобби",
  "car": "Автозапчасти а Аксессуары",
  "electronic": "Электроника",
  "children": "Детские товары",
  "clothes": "Одежда",
  "work": "Домашние животные",
  "sport": "Спорт, туризм и отдых",
  "change": "Обмен, отдам бесплатно",
  "other": "Другое",
};

const servicesMenu = [
  ['🏫 ', 'Посольства'],
  ['🍝 ', 'Рестораны/Кафе'],
  ['🍞 ', 'Продукты'],
  ['📱 ', 'Связь/Электроника'],
  ['📚 ', 'Образование'],
  ['🎉 ', 'Мероприятия/Фото/Видео'],
  ['📑 ', 'Документы/Переводы'],
  ['🚗 ', 'Авто купля/продажа'],
  ['❤️‍🩹 ', 'Здоровье'],
  ['💅🏼 ', 'Красота'],
  ['💰 ', 'Страхование/Финансы'],
  ['🚛 ', 'Трансфер/Переезд'],
  ['🛩 ', 'Туризм/Почта'],
  ['🔧 ', 'СТО/Тюнинг'],
  ['🕵️‍♂️ ', 'Другие услуги'],
  ['🛍 ', 'Другие товары'],
];

const servicesNames = {
  'all': 'Все',
  'embassy': 'Посольства',
  'food': 'Рестораны/Кафе',
  'shop': 'Продукты',
  'connect': 'Связь/Электроника',
  'study': 'Образование',
  'party': 'Мероприятия/Фото/Видео',
  'docs': 'Документы/Переводы',
  'cars': 'Авто купля/продажа',
  'health': 'Здоровье',
  'beauty': 'Красота',
  'insurance': 'Страхование/Финансы',
  'transport': 'Трансфер/Переезд',
  'travel': 'Туризм/Почта',
  'workshop': 'СТО/Тюнинг',
  'other': 'Другие услуги',
  'products': 'Другие товары',
};

const services = [
  'all',
  'embassy',
  'food',
  'shop',
  'connect',
  'study',
  'party',
  'docs',
  'cars',
  'health',
  'beauty',
  'insurance',
  'transport',
  'travel',
  'workshop',
  'other',
  'products',
];

const servicesMenuDB = [
  'embassy',
  'food',
  'shop',
  'connect',
  'study',
  'party',
  'docs',
  'cars',
  'health',
  'beauty',
  'insurance',
  'transport',
  'travel',
  'workshop',
  'other',
  'products',
];

const servicesN = [
  'food',
  'shop',
  'connect',
  'study',
  'party',
  'docs',
  'cars',
  'health',
  'beauty',
  'insurance',
  'transport',
  'travel',
  'workshop',
  'other',
  'products',
];

const servicesMenuNames = {
  'food': 'Рестораны/Кафе',
  'shop': 'Продукты',
  'connect': 'Связь/Электроника',
  'study': 'Образование',
  'party': 'Мероприятия/Фото/Видео',
  'docs': 'Документы/Переводы',
  'cars': 'Авто купля/продажа',
  'health': 'Здоровье',
  'beauty': 'Красота',
  'insurance': 'Страхование/Финансы',
  'transport': 'Трансфер/Переезд',
  'travel': 'Туризм/Почта',
  'workshop': 'СТО/Тюнинг',
  'other': 'Другие услуги',
  'products': 'Другие товары',
};

// const servicesMenuDB = {
//   '🏫   Посольства': 'embassy',
//   '🍲   Рестораны/Кафе': 'food',
//   '🍞   Продукты': 'shop',
//   '📱   Связь/Электроника': 'connect',
//   '📚  Образование': 'study',
//   '🥳   Мероприятия/Фото/Видео': 'party',
//   '📑   Документы/Переводы': 'docs',
//   '🚗   Авто купля/продажа': 'cars',
//   '💅🏼   Красота/Здоровье': 'health',
//   '🚛   Трансфер/Переезд': 'transport',
//   '✈️   Туризм/Почта': 'travel',
//   '🧑🏻‍🔧   СТО/Тюнинг': 'workshop',
//   '🥷   Другие услуги': 'other',
//   '🪆   Другие товары': 'products',
// };

const ageFrom = ['20', '25', '30', '35', '40'];
const ageTo = ['25', '30', '35', '40', '45', '50', '55', '60', '65', '70'];

var serviceImagesCount = 0;
var advImagesCount = 0;
var serviceImagesLinks = [];
var advImagesLinks = [];
