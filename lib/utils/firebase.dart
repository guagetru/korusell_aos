import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:work_app/model/service.dart';

import '../model/adv.dart';

Future<Adv?> getMyAdv(String uid) async {
  // print('UID: ' + uid);
  final doc = FirebaseFirestore.instance.collection('adv').doc(uid);
  final snapshot = await doc.get();
  if (snapshot.exists) {
    // print('SNAPSHOT EXISTS!');
    // print(Adv.fromJson(snapshot.data()!));
    return Adv.fromJson(snapshot.data()!);
  }
  return null;
}

Future<Service?> getService(String uid) async {
  // print('UID: ' + uid);
  final doc = FirebaseFirestore.instance.collection('services').doc(uid);
  final snapshot = await doc.get();
  if (snapshot.exists) {
    // print('SNAPSHOT EXISTS!');
    // print(Adv.fromJson(snapshot.data()!));
    return Service.fromJson(snapshot.data()!);
  }
  return null;
}
