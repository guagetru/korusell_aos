import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:geolocator/geolocator.dart';
import 'package:work_app/utils/constants.dart';
import 'package:work_app/utils/functions.dart';
import 'package:work_app/widgets/advertisement/adv_list.dart';
import 'package:work_app/widgets/auth/check_view.dart';
// import 'package:work_app/widgets/chat/chat.dart';
import 'package:work_app/widgets/map/map_view.dart';
import 'package:work_app/widgets/service/service_menu.dart';
import './widgets/work/work_list.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

void main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  try {
    await Firebase.initializeApp(options: firebaseOptions);
  } catch (e) {
    // ignore: avoid_print
    print(e);
  }

  if (FirebaseAuth.instance.currentUser != null) await updateLastLogin();
  // Position? position;
  // requestLocationPermission().then((value) => position = value, onError: (position = null));
  Position? position = await requestLocationPermission();
  // WidgetsFlutterBinding.ensureInitialized();
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
        primaryColor: Colors.black,
        // colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.amber)
        // .copyWith(secondary: Colors.amber),
        // cardColor: Color.fromRGBO(64, 75, 96, .9),
        // errorColor: Colors.black45,
        // buttonColor: Colors.white,
        // accentColor: Colors.white,
        fontFamily: 'Quicksand',
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: const TextStyle(
                color: Colors.black,
                fontFamily: 'Open Sans',
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
              bodyText1: const TextStyle(
                color: Colors.black,
                fontFamily: 'Open Sans',
                fontSize: 16,
              ),
              button: const TextStyle(
                color: Colors.black,
              ),
            ),
        appBarTheme: AppBarTheme(
          titleTextStyle: ThemeData.light()
              .textTheme
              .copyWith(
                headline6: const TextStyle(
                  fontFamily: 'Open Sans',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              )
              .headline6,
        )),
    home: BottomNavigation(position),
  ));
}

class BottomNavigation extends StatefulWidget {
  final Position? position;
  const BottomNavigation(this.position, {Key? key}) : super(key: key);
  // const BottomNavigation({Key? key}) : super(key: key);

  @override
  State<BottomNavigation> createState() => _BottomNavigation();
}

class _BottomNavigation extends State<BottomNavigation> {
  final GlobalKey<NavigatorState> firstTabNavKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> secondTabNavKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> thirdTabNavKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> fourthTabNavKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> fifthTabNavKey = GlobalKey<NavigatorState>();
  // final GlobalKey<NavigatorState> sixthTabNavKey = GlobalKey<NavigatorState>();

  late CupertinoTabController tabController;

  @override
  void initState() {
    super.initState();

    tabController = CupertinoTabController(initialIndex: 0);
    FlutterNativeSplash.remove();
  }

  @override
  Widget build(BuildContext context) {
    final listOfKeys = [
      firstTabNavKey,
      secondTabNavKey,
      thirdTabNavKey,
      fourthTabNavKey,
      fifthTabNavKey,
      // sixthTabNavKey
    ];
    List homeScreenList = [
      const ServiceMenu(),
      MapView(widget.position),
      const AdvList(),
      const WorkList(),
      // const Chat(),
      const CheckView(),
    ];
    return WillPopScope(
      onWillPop: () async {
        return !await listOfKeys[tabController.index].currentState!.maybePop();
      },
      child: CupertinoTabScaffold(
        controller: tabController,
        tabBar: CupertinoTabBar(
          iconSize: 22,
          activeColor: Colors.black,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.house_fill),
              label: 'Услуги',
            ),
            BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.map_fill),
              label: 'Карта',
            ),
            BottomNavigationBarItem(
              activeIcon: ImageIcon(AssetImage('assets/images/carrot.png')),
              icon: ImageIcon(AssetImage('assets/images/carrot.png')),
              // icon: ImageIcon(AssetImage('assets/images/carrot.png')),
              // Icon(CupertinoIcons.color_filter),
              label: 'Морковка',
            ),
            BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.wrench),
              label: 'Работа',
            ),
            // BottomNavigationBarItem(
            //   icon: Icon(CupertinoIcons.chat_bubble),
            //   label: 'Беседка',
            // ),
            BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.profile_circled),
              label: 'Профиль',
            ),
          ],
        ),
        tabBuilder: (BuildContext context, int index) {
          return CupertinoTabView(
            navigatorKey: listOfKeys[index],
            builder: (context) {
              return homeScreenList[index];
            },
          );
        },
      ),
    );
  }
}
