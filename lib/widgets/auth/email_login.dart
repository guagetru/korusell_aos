import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/_elements/buttons.dart';

class EmailLogin extends StatefulWidget {
  const EmailLogin({Key? key}) : super(key: key);

  @override
  _EmailLoginState createState() => _EmailLoginState();
}

class _EmailLoginState extends State<EmailLogin> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool isLoggingIn = false;

  _login() async {
    setState(() {
      isLoggingIn = true;
    });
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: _emailController.text, password: _passwordController.text);
      Navigator.of(context).pop();
      // Navigator.of(context)
      //     .push(MaterialPageRoute(builder: (context) => CheckView()));
    } on FirebaseAuthException catch (e) {
      var message = '';

      switch (e.code) {
        case 'invalid-email':
          message = 'Неверный email';
          break;
        case 'user-disabled':
          message = 'Пользователь заблокирован. Обратитесь к разработчикам';
          break;
        case 'user-not-found':
          message = 'Пользователь с таким email не найден';
          break;
        case 'wrong-password':
          message = 'Неверный пароль';
          break;
      }

      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Не удалось войти'),
                content: Text(message),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('Ok'))
                ],
              ));
    } finally {
      setState(() {
        isLoggingIn = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: 'Вход через Email'),
      body: Form(
        key: _formKey,
        child: Center(
          heightFactor: 0.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Введите Ваш email и пароль',
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 20),
              ),
              const SizedBox(height: 25),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 17),
                child: TextFormField(
                  validator: _requiredValidator,
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: const InputDecoration(
                    labelStyle: TextStyle(fontSize: 13),
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                    isCollapsed: true,
                    label: Text('Email'),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),

              // CustomTextField(label: 'Email', controller: _emailController),
              const SizedBox(
                height: 16,
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 17),
                child: TextFormField(
                  validator: _requiredValidator,
                  obscureText: true,
                  controller: _passwordController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: const InputDecoration(
                    labelStyle: TextStyle(fontSize: 13),
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                    isCollapsed: true,
                    label: Text('Пароль'),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              if (!isLoggingIn)
                SubmitButton(
                  onPressed: () {
                    if (_formKey.currentState != null &&
                        _formKey.currentState!.validate()) {
                      _login();
                    }
                  },
                  text: 'Войти',
                ),
              if (isLoggingIn) ...[
                const SizedBox(
                  height: 16,
                ),
                const Center(
                  child: CircularProgressIndicator(),
                ),
              ]
            ],
          ),
        ),
      ),
    );
  }

  String? _requiredValidator(String? text) {
    if (text == null || text.trim().isEmpty) {
      return 'Пожалуйста, заполните это поле';
    }
    return null;
  }
}
