import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import '../../utils/functions.dart';
import '../_elements/buttons.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();

  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  bool loading = false;
  String imageUrl = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: 'Регистрация'),
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            const SizedBox(
              height: 15,
            ),
            // UserImage(onFileChanged: (imageUrl) {
            //   setState(() {
            //     this.imageUrl = imageUrl;
            //   });
            // }),
            const SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: TextFormField(
                validator: _requiredValidator,
                controller: _nameController,
                decoration: const InputDecoration(
                  label: Text('Имя'),
                  labelStyle: TextStyle(fontSize: 13),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                  isCollapsed: true,
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            const SizedBox(height: 15),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: TextFormField(
                validator: _requiredValidator,
                controller: _emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(
                  label: Text('Email'),
                  labelStyle: TextStyle(fontSize: 13),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                  isCollapsed: true,
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            const SizedBox(height: 15),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: TextFormField(
                validator: _requiredValidator,
                controller: _phoneController,
                keyboardType: TextInputType.phone,
                decoration: const InputDecoration(
                  label: Text('Номер Телефона'),
                  labelStyle: TextStyle(fontSize: 13),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                  isCollapsed: true,
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            const SizedBox(height: 15),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: TextFormField(
                validator: _requiredValidator,
                controller: _passwordController,
                obscureText: true,
                decoration: const InputDecoration(
                  label: Text('Пароль'),
                  labelStyle: TextStyle(fontSize: 13),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                  isCollapsed: true,
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            const SizedBox(height: 15),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: TextFormField(
                validator: _confirmPasswordValidator,
                controller: _confirmPasswordController,
                obscureText: true,
                decoration: const InputDecoration(
                  label: Text('Повторите пароль'),
                  labelStyle: TextStyle(fontSize: 13),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                  isCollapsed: true,
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            const SizedBox(height: 30),
            if (loading) ...[
              const Center(child: CircularProgressIndicator()),
            ],
            if (!loading) ...[
              SubmitButton(
                text: 'Зарегистрироваться',
                onPressed: () {
                  if (_formKey.currentState != null &&
                      _formKey.currentState!.validate()) {
                    _signUp();
                  }
                },
                // padding: 16,
              ),
            ]
          ],
        ),
      ),
    );
  }

  String? _requiredValidator(String? text) {
    if (text == null || text.trim().isEmpty) {
      return 'Пожалуйста, заполните это поле';
    }
    return null;
  }

  String? _confirmPasswordValidator(String? password) {
    if (password == null || password.trim().isEmpty) {
      return 'Пожалуйста, заполните это поле';
    }

    if (_passwordController.text != password) {
      return 'Пароли не совпадают';
    }
    return null;
  }

  Future _signUp() async {
    setState(() {
      loading = true;
    });

    var date = dateForCreateAdv();
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: _emailController.text, password: _passwordController.text);

      await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser?.uid)
          .set({
        'created_date': date,
        'last_login': date,
        'uid': FirebaseAuth.instance.currentUser?.uid,
        'email': _emailController.text,
        'imageUrl': imageUrl,
        'name': _nameController.text,
        'phone': _phoneController.text,
      });

      await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Регистрация прошла успешно!'),
                // content: Text('Теперь Вы можете войти'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('OK')),
                ],
              ));
      Navigator.of(context).pop();
    } on FirebaseAuthException catch (e) {
      _handleSignUpError(e);
      setState(() {
        loading = false;
      });
    }
  }

  void _handleSignUpError(FirebaseAuthException e) {
    String messageToDisplay;
    switch (e.code) {
      case 'email-already-in-use':
        messageToDisplay = 'Этот email уже зарегистрирован';
        break;
      case 'invalid-email':
        messageToDisplay = 'Введен невеный email';
        break;
      case 'operation-not-allowed':
        messageToDisplay = 'Недоступная операция';
        break;
      case 'weak-password':
        messageToDisplay = 'Слижком слабый пароль';
        break;
      default:
        messageToDisplay = 'Неизвестная ошибка';
        break;
    }

    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: const Text('Ошибка при регистрации'),
              content: Text(messageToDisplay),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('OK')),
              ],
            ));
  }
}
