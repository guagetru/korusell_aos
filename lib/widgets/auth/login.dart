import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:work_app/widgets/auth/check_view.dart';
import 'package:work_app/widgets/auth/email_login.dart';
import 'package:work_app/widgets/auth/login_text.dart';
import '../../utils/functions.dart';
import '../_elements/buttons.dart';
import './sign_up.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var loading = false;
  var date = dateForCreateAdv();
  void _loginWithFacebook() async {
    setState(() {
      loading = true;
    });

    try {
      final facebookLoginResult = await FacebookAuth.instance
          .login(permissions: ["email", "public_profile"]);
      final userData = await FacebookAuth.instance.getUserData();
      final facebookAuthCredential = FacebookAuthProvider.credential(
          facebookLoginResult.accessToken!.token);
      await FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);

      final String imageUrl = userData['picture']['data']['url'] ?? '';

      await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser?.uid ?? userData['uid'])
          .set({
        'created_date': date,
        'last_login': date,
        'uid': FirebaseAuth.instance.currentUser?.uid ?? "",
        'email': userData['email'] ?? 'no@facebook.email',
        'imageUrl': imageUrl,
        'name': userData['name'],
        'phone': FirebaseAuth.instance.currentUser!.phoneNumber ?? "",
      });

      await saveImageToFirebaseStorageFromUrl(Uri.parse(imageUrl));

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => const CheckView()),
          (route) => false);
    } on FirebaseAuthException catch (e) {
      var title = '';
      switch (e.code) {
        case 'account-exists-with-different-credential':
          title = 'Этот аккаунт создан через другой провайдер';
          break;
        case 'invalid-credential':
          title = 'Неизвестная ошибка';
          break;
        case 'operation-not-allowed':
          title = 'Недоступная операция';
          break;
        case 'user-disabled':
          title = 'Пользователь заблокирован. Обратитесь к разработчикам';
          break;
        case 'user-not-found':
          title = 'Пользователь не найден';
          break;
      }

      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Не удалось войти через Facebook'),
                content: Text(title),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('Ok'))
                ],
              ));
    } catch (e) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Не удалось войти через Facebook'),
                content: const Text('Неизвестная ошибка. Попробуйте позже.'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('Ok'))
                ],
              ));
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  void _logInWithGoogle() async {
    setState(() {
      loading = true;
    });
    final googleSignIn = GoogleSignIn();
    try {
      final googleSignInAccount = await googleSignIn.signIn();
      if (googleSignInAccount == null) {
        setState(() {
          loading = false;
        });
        return;
      }

      final googleSignInAuthentication =
          await googleSignInAccount.authentication;
      final credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );
      // print('QQQQ');
      // print(credential);
      final imageUrl = googleSignInAccount.photoUrl ?? '';
      await FirebaseAuth.instance.signInWithCredential(credential);
      await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser?.uid)
          .set({
        'created_date': date,
        'last_login': date,
        'uid': FirebaseAuth.instance.currentUser?.uid,
        'email': googleSignInAccount.email,
        'imageUrl': imageUrl,
        'name': googleSignInAccount.displayName,
        'phone': FirebaseAuth.instance.currentUser!.phoneNumber ?? '',
      });

      await saveImageToFirebaseStorageFromUrl(Uri.parse(imageUrl));

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => const CheckView()),
          (route) => false);
    } on FirebaseAuthException catch (e) {
      var title = '';
      switch (e.code) {
        case 'account-exists-with-different-credential':
          title = 'Этот аккаунт создан через другой провайдер';
          break;
        case 'invalid-credential':
          title = 'Неизвестная ошибка. Повторите позже';
          break;
        case 'operation-not-allowed':
          title = 'Недоступная операция';
          break;
        case 'user-disabled':
          title = 'Пользователь заблокирован. Обратитесь к разработчикам';
          break;
        case 'user-not-found':
          title = 'Пользователь не найден';
          break;
      }

      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Не удалось войти через Google'),
                content: Text(title),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('Ok'))
                ],
              ));
    } catch (e) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Не удалось войти через Google'),
                content: const Text('Неизвестная ошибка: Повторите позже.'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('Ok'))
                ],
              ));
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      heightFactor: 0.9,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (loading) ...[
            const SizedBox(height: 15),
            const Center(
              child: CircularProgressIndicator(),
            ),
          ],
          if (!loading) ...[
            const LoginText('Войти в Korusell'),
            LoginButton(
              color: Colors.blue,
              image:
                  const AssetImage('assets/images/social/facebook_login.png'),
              text: 'Вход через Facebook',
              textColor: Colors.white,
              onPressed: () {
                _loginWithFacebook();
              },
            ),
            LoginButton(
              color: Colors.white,
              image: const AssetImage('assets/images/social/google_login.png'),
              text: 'Вход через Google',
              textColor: Colors.black54,
              onPressed: () {
                _logInWithGoogle();
              },
            ),
            LoginButton(
              color: Colors.white,
              image: const AssetImage('assets/images/social/email_login.png'),
              text: 'Вход через Email',
              textColor: Colors.black54,
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const EmailLogin()));
              },
            ),
            const SizedBox(
              height: 20,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: Divider(
                  // color: Colors.grey.shade500,
                  ),
            ),
            const SizedBox(
              height: 20,
            ),
            SubmitButton(
                text: 'Зарегистрироваться',
                // padding: 50,
                onPressed: () {
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => const SignUp()));
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => const SignUp()));
                })
          ]
        ],
      ),
    ));
  }
}
