import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:work_app/model/adv.dart';
import 'package:work_app/utils/constants.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/advertisement/adv_list_view.dart';
import 'adv_filter.dart';

class AdvList extends StatefulWidget {
  const AdvList({Key? key}) : super(key: key);

  @override
  _AdvListState createState() => _AdvListState();
}

class _AdvListState extends State<AdvList> {
  String category = "all";
  String city = "Все Города";
  String categoryName = "Все";
  callback(newCategory, newCity) {
    setState(() {
      category = newCategory;
      city = newCity;
    });
  }

  @override
  Widget build(BuildContext context) {
    var allAdvallCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .orderBy('createdAt', descending: true)
        .snapshots();
    var allAdvsomeCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .where('city', isEqualTo: city)
        .orderBy('createdAt', descending: true)
        .snapshots();
    var someAdvallCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .where('category', isEqualTo: category)
        .orderBy('createdAt', descending: true)
        .snapshots();
    var someAdvsomeCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .where('city', isEqualTo: city)
        .where('category', isEqualTo: category)
        .orderBy('createdAt', descending: true)
        .snapshots();
    var allAdvotherCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        // .where('city', whereNotIn: DBcities)
        .snapshots();
    var someAdvotherCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        // .where('city', whereNotIn: DBcities)
        .orderBy('city')
        .where('category', isEqualTo: category)
        .orderBy('createdAt', descending: true)
        .snapshots();

    var reference = someAdvsomeCity;
    if (category == "all") {
      if (city == "Другой Город") {
        reference = allAdvotherCity;
      } else if (city == "Все Города") {
        reference = allAdvallCity;
      } else {
        reference = allAdvsomeCity;
      }
    } else if (category != "all") {
      if (city == "Другой Город") {
        reference = someAdvotherCity;
      } else if (city == "Все Города") {
        reference = someAdvallCity;
      }
    }

    return Scaffold(
        // backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        appBar: MyAppBar(
          label: 'Объявления',
          actions: [
            IconButton(
              color: Colors.black,
              icon: const Icon(CupertinoIcons.line_horizontal_3_decrease),
              onPressed: () {
                showModalBottomSheet(
                    useRootNavigator: false,
                    context: context,
                    builder: (BuildContext context) {
                      return AdvFilter(category, city, callback);
                    });
              },
            ),
          ],
        ),
        body: StreamBuilder(
            stream: reference,
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) {
                // return Text('Пофиксить placeholder');
                return ListView.separated(
                  itemCount: 6,
                  itemBuilder: (BuildContext context, int index) {
                    return SizedBox(
                        height: MediaQuery.of(context).size.height * 0.13,
                        child: const InkWell());
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const Divider(
                        // color: Colors.black,
                        );
                  },
                );
              } else {
                var docs = snapshot.data!.docs
                  ..removeWhere((element) => element['category'] == 'work');
                if (city == 'Другой Город') {
                  docs = snapshot.data!.docs
                    ..removeWhere((element) =>
                        element['category'] == 'work' ||
                        element['city'] == 'Ансан' ||
                        element['city'] == 'Хвасонг' ||
                        element['city'] == 'Инчхон' ||
                        element['city'] == 'Сеул' ||
                        element['city'] == 'Сувон' ||
                        element['city'] == 'Асан' ||
                        element['city'] == 'Чхонан' ||
                        element['city'] == 'Чхонджу' ||
                        element['city'] == 'Пхёнтхэк' ||
                        element['city'] == 'Сосан' ||
                        element['city'] == 'Дунпо');
                }
                return ListView.separated(
                  itemCount: docs.length,
                  itemBuilder: (BuildContext context, int index) {
                    final QueryDocumentSnapshot<Object?>? data = docs[index];
                    Adv adv = Adv(
                        age: data?.get('age'),
                        category: data?.get('category'),
                        city: data?.get('city'),
                        createdAt: data?.get('createdAt'),
                        description: data?.get('description'),
                        gender: data?.get('gender'),
                        id: data?.get('id'),
                        images: data?.get('images'),
                        isActive: data?.get('isActive'),
                        name: data?.get('name'),
                        phone: data?.get('phone'),
                        price: data?.get('price'),
                        shift: data?.get('shift'),
                        subcategory: data?.get('subcategory'),
                        uid: data?.get('uid'),
                        updatedAt: data?.get('updatedAt'),
                        visa: data?.get('visa'));
                    String imageUrl = 'gs://korusell.appspot.com/advImages/' +
                        adv.uid +
                        'ADV' +
                        '0' +
                        '.jpg';
                    return AdvView(
                      adv: adv,
                      imageUrl: imageUrl,
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const Divider();
                  },
                );
              }
            }));
  }
}
