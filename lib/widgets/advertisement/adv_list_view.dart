import 'package:flutter/material.dart';

import '../../model/adv.dart';
import '../../utils/functions.dart';
import '../../utils/image_controller/custom_image.dart';
import 'adv_details.dart';

class AdvView extends StatelessWidget {
  final Adv adv;
  final String imageUrl;

  final bool cache;

  const AdvView({
    Key? key,
    this.cache = true,
    required this.adv,
    required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: double.parse(adv.isActive),
      child: InkWell(
        // margin: new EdgeInsets.symmetric(
        // horizontal: 10.0),
        child: ListTile(
          title: Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  CustomImage(
                      imageUrl,
                      MediaQuery.of(context).size.width * 0.25,
                      MediaQuery.of(context).size.width * 0.25,
                      cache),
                ],
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width * 0.6,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      formattedDate(adv.createdAt),
                      style: const TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Text(
                      adv.name,
                      maxLines: 2,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 15),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Text(
                      'Город:  ' + adv.city,
                      style: const TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Text(
                      adv.price,
                      style: const TextStyle(fontSize: 16),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ],
                ),
              )
            ],
          ),
          onTap: () => {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AdvDetails(adv: adv)))
          },
        ),
      ),
    );
  }
}
