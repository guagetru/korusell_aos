import 'package:flutter/material.dart';
import 'package:work_app/model/adv.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'adv_details_view.dart';

class AdvDetails extends StatelessWidget {
  final Adv adv;

  const AdvDetails({Key? key, required this.adv}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: adv.name),
      body: SingleChildScrollView(
        child: Column(
          children: [
            AdvDetailsView(
              adv: adv,
            ),
            const SizedBox(height: 80),
          ],
        ),
      ),
    );
  }
}
