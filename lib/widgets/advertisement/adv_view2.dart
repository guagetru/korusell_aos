import 'package:flutter/material.dart';

import '../../model/adv.dart';
import '../../utils/functions.dart';
import '../../utils/image_controller/custom_image.dart';

class AdvView2 extends StatefulWidget {
  final Adv adv;
  final String imageUrl;
  final Widget widget;
  final bool cache;

  const AdvView2(
      {Key? key,
      this.cache = true,
      required this.adv,
      required this.imageUrl,
      required this.widget})
      : super(key: key);

  @override
  State<AdvView2> createState() => _AdvView2State();
}

class _AdvView2State extends State<AdvView2> {
  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: double.parse(widget.adv.isActive),
      child: InkWell(
        // margin: new EdgeInsets.symmetric(
        // horizontal: 10.0),
        child: ListTile(
          title: Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  CustomImage(
                      widget.imageUrl,
                      MediaQuery.of(context).size.width * 0.25,
                      MediaQuery.of(context).size.width * 0.25,
                      widget.cache),
                ],
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width * 0.6,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      formattedDate(widget.adv.createdAt),
                      style: const TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Text(
                      widget.adv.name,
                      maxLines: 2,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 15),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Text(
                      'Город:  ' + widget.adv.city,
                      style: const TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Text(
                      widget.adv.price,
                      style: const TextStyle(fontSize: 16),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ],
                ),
              )
            ],
          ),
          onTap: () => {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => widget.widget)),
          },
        ),
      ),
    );
  }
}
