import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:work_app/widgets/_elements/business.dart';
import 'package:work_app/widgets/_elements/owner.dart';

import '../../model/adv.dart';
import '../../utils/functions.dart';
import '../../utils/image_controller/custom_image.dart';
import '../_elements/buttons.dart';

class AdvDetailsView extends StatefulWidget {
  const AdvDetailsView({
    Key? key,
    required this.adv,
  }) : super(key: key);

  final Adv adv;

  @override
  State<AdvDetailsView> createState() => _AdvDetailsViewState();
}

class _AdvDetailsViewState extends State<AdvDetailsView> {
  double currentIndexPage = 0.0;

  Widget carousel() {
    return Column(
      children: [
        Center(
          child: CarouselSlider.builder(
            itemCount: int.parse(widget.adv.images),
            itemBuilder:
                (BuildContext context, int itemIndex, int pageViewIndex) {
              return CustomImage(
                  'gs://korusell.appspot.com/advImages/' +
                      widget.adv.uid +
                      'ADV' +
                      itemIndex.toString() +
                      '.jpg',
                  MediaQuery.of(context).size.height * 0.42,
                  double.infinity / 2,
                  true);
            },
            options: CarouselOptions(
              onPageChanged: ((index, reason) => setState(() {
                    currentIndexPage = index.ceilToDouble();
                  })),
              enableInfiniteScroll: false,
              autoPlay: false,
              enlargeCenterPage: true,
              viewportFraction: 1,
              aspectRatio: 1,
              initialPage: 0,
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Center(
          child: DotsIndicator(
            decorator: const DotsDecorator(
              activeColor: Colors.blue,
              // activeSize: Size.square(10),
              size: Size.square(7),
              spacing: EdgeInsets.symmetric(horizontal: 4),
            ),
            mainAxisSize: MainAxisSize.min,
            dotsCount: int.parse(widget.adv.images),
            position: currentIndexPage,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        (int.parse(widget.adv.images) > 1)
            ? carousel()
            : CustomImage(
                'gs://korusell.appspot.com/advImages/' +
                    widget.adv.uid +
                    'ADV0' +
                    '.jpg',
                MediaQuery.of(context).size.height * 0.42,
                double.infinity / 2,
                true),
        Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Owner(widget.adv.uid.substring(0, 28)),
              Business(widget.adv.uid.substring(0, 28)),
              Text(
                formattedDate(widget.adv.createdAt),
                style: const TextStyle(fontSize: 13, color: Colors.grey),
              ),
              const SizedBox(height: 10),
              Text(
                widget.adv.name,
                maxLines: 2,
                style:
                    const TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              Row(
                children: [const Text('Город:  '), Text(widget.adv.city)],
              ),
              const SizedBox(height: 10),
              if (widget.adv.price != '')
                Text(
                  widget.adv.price,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              const SizedBox(height: 10),
              const Divider(
                height: 10,
                thickness: 1,
              ),
              const SizedBox(height: 10),
              if (widget.adv.description != '')
                const Text(
                  'Описание',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              const SizedBox(height: 5),
              Text(widget.adv.description),
              const SizedBox(height: 10),
              if (widget.adv.description != '')
                const Divider(
                  height: 10,
                  thickness: 1,
                ),
              const SizedBox(height: 20),
            ],
          ),
        ),
        if (widget.adv.phone != "")
          Center(
            child: SubmitButton(
                text: widget.adv.phone,
                onPressed: () => launch("tel://${widget.adv.phone}")),
          ),
      ],
    );
  }
}
