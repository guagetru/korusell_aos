import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';
import 'package:work_app/model/service.dart';
import 'package:work_app/widgets/service/service_details.dart';

class Business extends StatelessWidget {
  final String uid;
  const Business(this.uid, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String businessUrl = 'gs://korusell.appspot.com/images/' +
        uid.substring(0, 28) +
        '0' +
        '.jpg';
    return FutureBuilder<DocumentSnapshot>(
        future:
            FirebaseFirestore.instance.collection('services').doc(uid).get(),
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text("... Загрузка");
          }

          if (snapshot.hasData && !snapshot.data!.exists) {
            return const SizedBox();
          }

          if (snapshot.connectionState != ConnectionState.done) {
            // Map<String, dynamic> data =
            //     snapshot.data!.data() as Map<String, dynamic>;
            return TextButton.icon(
              onPressed: () {
                // Navigator.of(context).push(MaterialPageRoute(
                //     builder: (context) =>
                //         ServiceDetails(Service.fromJson(data))));
                // showModalBottomSheet(
                //   useRootNavigator: false,
                //   context: context,
                //   builder: (BuildContext context) {
                //     return ServiceDetails(Service.fromJson(data));
                //   },
                // );
              },
              icon: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image:
                              FirebaseImage(businessUrl, shouldCache: true)))),
              label: const Text("      "),
            );
          }
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          return TextButton.icon(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) =>
                      ServiceDetails(Service.fromJson(data))));
              // showModalBottomSheet(
              //   useRootNavigator: false,
              //   context: context,
              //   builder: (BuildContext context) {
              //     return ServiceDetails(Service.fromJson(data));
              //   },
              // );
            },
            icon: Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: FirebaseImage(businessUrl, shouldCache: true)))),
            label: data['name'] != ''
                ? Text(
                    data['name'],
                    overflow: TextOverflow.ellipsis,
                  )
                : const Text("Бизнес"),
          );
        });
  }
}
