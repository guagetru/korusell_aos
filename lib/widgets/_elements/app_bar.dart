import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget with PreferredSizeWidget {
  final String label;
  final List<Widget>? actions;

  @override
  final Size preferredSize;

  MyAppBar({Key? key, required this.label, this.actions})
      : preferredSize = const Size.fromHeight(50.0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: const IconThemeData(
        color: Colors.black,
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(
        label,
        style: const TextStyle(color: Colors.black),
      ),
      actions: actions,
    );
  }
}
