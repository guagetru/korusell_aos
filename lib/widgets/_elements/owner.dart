import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';

import '../profile/contact_view.dart';

class Owner extends StatelessWidget {
  final String uid;
  const Owner(this.uid, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String avatarUrl = 'gs://korusell.appspot.com/avatars/' + uid + '.jpg';
    return FutureBuilder<DocumentSnapshot>(
        future: FirebaseFirestore.instance.collection('users').doc(uid).get(),
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text("... Загрузка");
          }

          if (snapshot.hasData && !snapshot.data!.exists) {
            return const SizedBox();
          }

          if (snapshot.connectionState != ConnectionState.done) {
            // Map<String, dynamic> data =
            //     snapshot.data!.data() as Map<String, dynamic>;

            return TextButton.icon(
              onPressed: () {
                showModalBottomSheet(
                  useRootNavigator: false,
                  context: context,
                  builder: (BuildContext context) {
                    return ContactView(uid, avatarUrl);
                  },
                );
              },
              icon: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: FirebaseImage(avatarUrl, shouldCache: true)))),
              label: const Text("         "),
            );
          }

          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          Image image = Image.network(data['imageUrl'] ?? '');
          if (data['imageUrl'] == null || data['imageUrl'] == '') {
            image = Image(
              image: FirebaseImage(avatarUrl,
                  shouldCache: true,
                  cacheRefreshStrategy: CacheRefreshStrategy.BY_METADATA_DATE),
            );
          }
          return TextButton.icon(
            onPressed: () {
              showModalBottomSheet(
                useRootNavigator: false,
                context: context,
                builder: (BuildContext context) {
                  return ContactView(uid, avatarUrl);
                },
              );
            },
            icon: Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: image.image,
                    ))),
            label: data['name'] != ''
                ? Text(data['name'])
                : const Text("Контактное лицо"),
          );
        });
  }
}
