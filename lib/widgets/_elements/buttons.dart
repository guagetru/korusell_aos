import 'package:flutter/material.dart';

class SubmitButton extends StatelessWidget {
  const SubmitButton(
      {Key? key,
      required this.onPressed,
      this.text = 'Принять',
      this.padding = 17,
      this.color = Colors.blue})
      : super(key: key);

  final VoidCallback onPressed;
  final String text;
  final double padding;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: padding),
      child: SizedBox(
        width: double.infinity,
        child: ElevatedButton(
            style:
                ElevatedButton.styleFrom(primary: Colors.white, elevation: 1),
            onPressed: () {
              onPressed();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  text,
                  style: TextStyle(
                    color: color,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            )),

        // OutlinedButton(
        //   onPressed: () {
        //     if (onPressed != null) {
        //       onPressed();
        //     }
        //   },
        //   child: Text(
        //     text,
        //     style: TextStyle(
        //       fontWeight: FontWeight.bold,
        //       color: color,
        //     ),
        //   ),
        // ),
      ),
    );
  }
}

class MainButton extends StatelessWidget {
  final String text;
  final VoidCallback function;
  final String sufix;
  const MainButton(this.text, this.function, {Key? key, this.sufix = ''})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 17),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.white,
            elevation: 1,
          ),
          onPressed: () {
            function();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                text,
                style: const TextStyle(color: Colors.black),
              ),
              Row(
                children: [
                  Text(
                    sufix,
                    maxLines: 1,
                    style: const TextStyle(color: Colors.grey),
                  ),
                  const SizedBox(
                    width: 7,
                  ),
                  const Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                    color: Colors.grey,
                  ),
                ],
              )
            ],
          )),
    );
  }
}

class LoginButton extends StatelessWidget {
  final Color color;
  final Color textColor;
  final ImageProvider image;
  final String text;
  final VoidCallback onPressed;

  const LoginButton({
    Key? key,
    required this.color,
    required this.image,
    required this.text,
    required this.onPressed,
    this.textColor = Colors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 17, right: 17),
      child: GestureDetector(
        onTap: onPressed,
        child: Container(
          height: 45,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.7),
                spreadRadius: 0,
                blurRadius: 1,
                offset: const Offset(0, 0), // changes position of shadow
              ),
            ],
            color: color,
            // border: Border.all(color: color),

            borderRadius: BorderRadius.circular(5),
          ),
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              const SizedBox(
                width: 5,
              ),
              Image(
                image: image,
                width: 25,
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(text,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 15,
                          fontWeight: FontWeight.bold)),
                  const SizedBox(
                    width: 35,
                  ),
                ],
              ))
            ],
          ),
        ),
      ),
    );
  }
}
