import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';

import '../../model/service.dart';
import '../../utils/constants.dart';
import '../../utils/image_controller/service_image.dart';
import '../_elements/buttons.dart';

class EditMyService extends StatefulWidget {
  final Service service;

  const EditMyService(this.service, {Key? key}) : super(key: key);

  @override
  State<EditMyService> createState() => _EditMyServiceState();
}

class _EditMyServiceState extends State<EditMyService> {
  final _formKey = GlobalKey<FormState>();

  bool loading = false;
  bool isVisible = false;
  bool showLinks = false;

  String city = 'Ансан';
  String otherCity = '';
  String category = 'food';
  String imageUrl = '';
  String address = '';
  String description = '';
  String id = '';
  String images = '';
  String latitude = '';
  String longitude = '';
  String name = '';
  String phone = '';
  List<dynamic> social = ['', '', '', '', ''];

  @override
  void initState() {
    name = widget.service.name;
    address = widget.service.address;
    description = widget.service.description;
    phone = widget.service.phone;
    category = widget.service.category;
    images = widget.service.images;
    serviceImagesCount = int.parse(images);
    social = widget.service.social;
    if (citiesN.contains(widget.service.city)) {
      city = widget.service.city;
    } else {
      city = 'Другой Город';
      otherCity = widget.service.city;
      isVisible = true;
    }

    super.initState();
  }

  CollectionReference services =
      FirebaseFirestore.instance.collection('services');
  double currentIndexPage = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: 'Мой Бизнес'),
      body: Form(
          key: _formKey,
          child: ListView(
            children: [
              const SizedBox(
                height: 15,
              ),
              ServiceImage(int.parse(widget.service.images)),
              const SizedBox(height: 15),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      initialValue: widget.service.name,
                      validator: _requiredValidator,
                      decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 13),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isCollapsed: true,
                        label: Text('Название Бизнеса'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => name = value,
                    ),
                    Row(
                      children: [
                        const Text('Город:  '),
                        DropdownButton(
                          underline: const Text(''),
                          value: city,
                          // icon: const Icon(Icons.keyboard_arrow_down),
                          items: citiesN.map((String items) {
                            return DropdownMenuItem(
                              child: Text(
                                items,
                                style: const TextStyle(fontSize: 14),
                              ),
                              value: items,
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              city = newValue!;
                              if (city == "Другой Город") {
                                isVisible = true;
                              } else {
                                isVisible = false;
                              }
                            });
                          },
                        ),
                      ],
                    ),
                    Visibility(
                      visible: isVisible,
                      child: TextFormField(
                        initialValue: otherCity,
                        validator: _requiredValidator,
                        decoration: const InputDecoration(
                          labelStyle: TextStyle(fontSize: 13),
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                          isCollapsed: true,
                          label: Text('Введите название Города'),
                          border: OutlineInputBorder(),
                        ),
                        onChanged: (value) => otherCity = value,
                      ),
                    ),
                    const SizedBox(height: 15),
                    Row(
                      children: [
                        const Text('Категории: '),
                        DropdownButton(
                          value: category,
                          underline: const Text(''),
                          items: servicesN.map((String items) {
                            return DropdownMenuItem(
                              child: Text(
                                servicesMenuNames[items]!,
                                style: const TextStyle(fontSize: 14),
                                overflow: TextOverflow.ellipsis,
                              ),
                              value: items,
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              category = newValue!;
                            });
                          },
                        ),
                      ],
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      validator: _requiredValidator,
                      keyboardType: TextInputType.phone,
                      initialValue: widget.service.phone,
                      decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 13),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isCollapsed: true,
                        label: Text('Номер Телефона'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => phone = value,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      initialValue: widget.service.address,
                      decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 13),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isCollapsed: true,
                        label: Text('Адрес'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => address = value,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      maxLines: 10,
                      minLines: 1,
                      initialValue: widget.service.description,
                      decoration: const InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 5),
                        isCollapsed: true,
                        labelStyle: TextStyle(fontSize: 13),
                        label: Text('Описание'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => description = value,
                    ),
                    Center(
                      child: TextButton(
                          onPressed: () {
                            setState(() {
                              showLinks = !showLinks;
                            });
                          },
                          child: const Text('Ссылки')),
                    ),
                    Visibility(
                      visible: showLinks,
                      child: Column(
                        children: [
                          TextFormField(
                            initialValue: social[0] == ''
                                ? 'https://www.facebook.com/'
                                : social[0],
                            decoration: const InputDecoration(
                              labelStyle: TextStyle(fontSize: 13),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              isCollapsed: true,
                              label: Text('Facebook'),
                              border: OutlineInputBorder(),
                            ),
                            onChanged: (value) => social[0] = value,
                          ),
                          const SizedBox(height: 10),
                          TextFormField(
                            initialValue: social[1] == ''
                                ? 'https://www.instagram.com/'
                                : social[1],
                            decoration: const InputDecoration(
                              labelStyle: TextStyle(fontSize: 13),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              isCollapsed: true,
                              label: Text('Instagram'),
                              border: OutlineInputBorder(),
                            ),
                            onChanged: (value) => social[1] = value,
                          ),
                          const SizedBox(height: 10),
                          TextFormField(
                            initialValue:
                                social[2] == '' ? 'https://t.me/' : social[2],
                            decoration: const InputDecoration(
                              labelStyle: TextStyle(fontSize: 13),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              isCollapsed: true,
                              label: Text('Telegram'),
                              border: OutlineInputBorder(),
                            ),
                            onChanged: (value) => social[2] = value,
                          ),
                          const SizedBox(height: 10),
                          TextFormField(
                            initialValue: social[3] == ''
                                ? 'https://www.youtube.com/'
                                : social[3],
                            decoration: const InputDecoration(
                              labelStyle: TextStyle(fontSize: 13),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              isCollapsed: true,
                              label: Text('Youtube'),
                              border: OutlineInputBorder(),
                            ),
                            onChanged: (value) => social[3] = value,
                          ),
                          const SizedBox(height: 10),
                          TextFormField(
                            initialValue:
                                social[4] == '' ? 'https://' : social[4],
                            decoration: const InputDecoration(
                              labelStyle: TextStyle(fontSize: 13),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              isCollapsed: true,
                              label: Text('Веб Страница'),
                              border: OutlineInputBorder(),
                            ),
                            onChanged: (value) => social[4] = value,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 30),
                    if (loading) ...[
                      const Center(child: CircularProgressIndicator()),
                    ],
                  ],
                ),
              ),
              if (!loading) ...[
                SubmitButton(
                  text: 'Обновить',
                  onPressed: () {
                    if (_formKey.currentState != null &&
                        _formKey.currentState!.validate()) {
                      _updateService();
                    }
                  },
                  // padding: 16,
                ),
              ]
            ],
          )),
    );
  }

  Future _updateService() async {
    setState(() {
      loading = true;
      if (city != 'Другой Город') {
        otherCity = city;
      }
    });
    if (social.isEmpty) {
      social = ['', '', '', '', ''];
    }
    String lat = '';
    String lon = '';

    String uid = FirebaseAuth.instance.currentUser!.uid;

    images = serviceImagesCount.toString();
    // deleteImages(serviceImagesCount);
    if (serviceImagesCount == 1) {
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}1.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}2.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}3.jpg')
          .delete();
    } else if (serviceImagesCount == 2) {
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}2.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}3.jpg')
          .delete();
    } else if (serviceImagesCount == 3) {
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}3.jpg')
          .delete();
    } else if (serviceImagesCount == 0) {
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}0.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}1.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}2.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/${uid}3.jpg')
          .delete();
    }

    try {
      try {
        List<Location> locations = await locationFromAddress(address);
        lat = locations[0].latitude.toString();
        lon = locations[0].longitude.toString();
      } on Exception catch (e) {
        // ignore: avoid_print
        print('Невозмонжно найти геолокацию адреса');
        // ignore: avoid_print
        print(e);
      }

      // var locations = await locationFromAddress(address);

      await FirebaseFirestore.instance.collection('services').doc(uid).set({
        'id': uid,
        'uid': uid,
        'address': address,
        'category': category,
        'city': otherCity,
        'description': description,
        'name': name,
        'phone': phone,
        'social': social,
        'images': images,
        'latitude': lat,
        'longitude': lon,
      });

      await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Бизнес успешно обновлен!'),
                // content: Text('Теперь Вы можете войти'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('OK')),
                ],
              ));
      Navigator.of(context).pop();
    } on FirebaseAuthException {
      // _handleSignUpError(e);
      setState(() {
        loading = false;
      });
    }
  }

  String? _requiredValidator(String? text) {
    if (text == null || text.trim().isEmpty) {
      return 'Пожалуйста, заполните это поле';
    }
    return null;
  }
}
