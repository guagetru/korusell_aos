import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import '../../model/user.dart' as u;
import '../_elements/buttons.dart';

class EditProfile extends StatefulWidget {
  final u.User user;
  const EditProfile(this.user, {Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _formKey = GlobalKey<FormState>();
  bool loading = false;
  bool loading2 = false;
  String email = '';
  String name = '';
  String phone = '';

  @override
  void initState() {
    name = widget.user.name;
    phone = widget.user.phone;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar(label: 'Управление Аккаунтом'),
        body: Form(
            key: _formKey,
            child: ListView(children: [
              const SizedBox(
                height: 15,
              ),
              Column(
                children: [
                  Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          TextFormField(
                            initialValue: widget.user.name,
                            validator: _requiredValidator,
                            decoration: const InputDecoration(
                              labelStyle: TextStyle(fontSize: 13),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              isCollapsed: true,
                              label: Text('Имя *'),
                              border: OutlineInputBorder(),
                            ),
                            onChanged: (value) => name = value,
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          TextFormField(
                            initialValue: widget.user.phone,
                            validator: _requiredValidator,
                            decoration: const InputDecoration(
                              labelStyle: TextStyle(fontSize: 13),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              isCollapsed: true,
                              label: Text('Номер Телефона *'),
                              border: OutlineInputBorder(),
                            ),
                            onChanged: (value) => phone = value,
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                        ],
                      )),
                  if (loading) ...[
                    const Center(child: CircularProgressIndicator()),
                  ],
                  if (!loading) ...[
                    SubmitButton(
                      text: 'Обновить Аккаунт',
                      onPressed: () {
                        if (_formKey.currentState != null &&
                            _formKey.currentState!.validate()) {
                          _updateAccount();
                        }
                      },
                      // padding: 16,
                    ),
                  ],
                  const SizedBox(
                    height: 30,
                  ),
                  if (loading2) ...[
                    const Center(child: CircularProgressIndicator()),
                  ],
                  if (!loading2) ...[
                    SubmitButton(
                      color: Colors.red,
                      text: 'Удалить Аккаунт',
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                                  title: const Text(
                                      'Ваш аккаунт будет удален! Вся информация будет утеряна!'),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          _deleteAccount();
                                          Navigator.of(context).pop();
                                        },
                                        child: const Text('Удалить')),
                                    TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: const Text('Отмена')),
                                  ],
                                ));
                      },
                      // padding: 16,
                    ),
                  ],
                  const SizedBox(
                    height: 30,
                  ),
                ],
              )
            ])));
  }

  Future _deleteAccount() async {
    setState(() {
      loading2 = true;
    });
    try {
      User currentUser = FirebaseAuth.instance.currentUser!;
      String uid = widget.user.uid;
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/avatars/$uid.jpg')
          .delete();

      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/images/$uid.jpg')
          .delete();
      await FirebaseFirestore.instance.collection('users').doc(uid).delete();
      await FirebaseFirestore.instance.collection('service2').doc(uid).delete();
      FirebaseAuth.instance.signOut();
      currentUser.delete();

      await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Аккаунт удален!'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('OK')),
                ],
              ));
      Navigator.of(context).pop();
    } on FirebaseAuthException {
      setState(() {
        loading2 = false;
      });
    }
  }

  Future _updateAccount() async {
    setState(() {
      loading = true;
    });
    try {
      // var locations = await locationFromAddress(address);
      String uid = widget.user.uid;

      await FirebaseFirestore.instance.collection('users').doc(uid).set({
        'uid': widget.user.uid,
        'email': widget.user.email,
        'created_date': widget.user.createdDate,
        'imageUrl': widget.user.imageUrl,
        'last_login': widget.user.lastLogin,
        'phone': phone,
        'name': name,
      });

      await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Данные обновлены!'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('OK')),
                ],
              ));
      Navigator.of(context).pop();
    } on FirebaseAuthException {
      setState(() {
        loading = false;
      });
    }
  }
}

String? _requiredValidator(String? text) {
  if (text == null || text.trim().isEmpty) {
    return 'Пожалуйста, заполните это поле';
  }
  return null;
}
