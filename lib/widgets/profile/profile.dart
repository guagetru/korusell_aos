import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:work_app/model/user.dart' as u;
import 'package:work_app/utils/image_controller/user_image.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/profile/chat.dart';
import 'package:work_app/widgets/profile/edit_profile.dart';

import '../_elements/buttons.dart';
import 'my_adv_list.dart';
import 'my_service.dart';

class ProfileView extends StatefulWidget {
  final String url;
  const ProfileView({Key? key, this.url = ''}) : super(key: key);

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
_signOut() async {
  await _firebaseAuth.signOut();
}

class _ProfileViewState extends State<ProfileView> {
  String imageUrl = '';

  @override
  Widget build(BuildContext context) {
    CollectionReference users = FirebaseFirestore.instance.collection('users');
    return Scaffold(
      appBar: MyAppBar(
        label: 'Моя страница',
      ),
      backgroundColor: Colors.grey[200],
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              StreamBuilder(
                stream: users
                    .doc(FirebaseAuth.instance.currentUser!.uid)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return const Text("Неизвестная ошибка");
                  }

                  if (snapshot.hasData && !snapshot.data!.exists) {
                    return const Text("Аккаунт удален");
                  }

                  if (snapshot.hasData) {
                    Map<String, dynamic> data =
                        snapshot.data?.data() as Map<String, dynamic>;

                    u.User user = u.User.fromJson(data);
                    imageUrl = user.imageUrl;
                    String name = user.name;
                    if (user.name.length > 13) {
                      name = name.substring(0, 13);
                    }
                    return Center(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 15,
                          ),
                          UserImage(
                            onFileChanged: (imageUrl) {
                              setState(() {
                                this.imageUrl = imageUrl;
                                // print(imageUrl);
                              });
                            },
                            imageUrl: imageUrl,
                          ),
                          CustomCard1(user: user),
                          const Divider(
                            height: 40,
                          ),
                          myAdvsButton(user.uid),
                          const SizedBox(
                            height: 10,
                          ),
                          myBusinessButton(user.uid),
                          const Divider(
                            height: 40,
                          ),
                          MainButton(
                            '👨🏻‍💻  Управление Аккаунтом',
                            (() => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EditProfile(user)))),
                            sufix: name,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          MainButton(
                              '📩 Обратная Связь',
                              (() => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const Chat())))),
                          const Divider(
                            height: 40,
                          ),
                        ],
                      ),
                    );
                  } else {
                    return const CircularProgressIndicator();
                  }

                  // u.User dummyUser = u.User(
                  //   uid: '',
                  //   name: '',
                  //   phone: '',
                  //   email: '',
                  //   imageUrl: '',
                  //   createdDate: Timestamp.now(),
                  //   lastLogin: Timestamp.now(),
                  // );
                  // return Center(
                  //   child: Column(
                  //     children: [
                  //       const SizedBox(
                  //         height: 15,
                  //       ),
                  //       CircleAvatar(
                  //         radius: 40,
                  //         backgroundColor: Colors.black12,
                  //       ),
                  //       Text(
                  //         'Изменить фото',
                  //         style: TextStyle(fontWeight: FontWeight.bold),
                  //       ),

                  //       // if (imageUrl != "")
                  //       //   CircleAvatar(

                  //       //       radius: 40,
                  //       //       backgroundImage:
                  //       //           NetworkImage(data['imageUrl'])),
                  //       // if (imageUrl == "")

                  //       //   CircleAvatar(
                  //       //     backgroundColor: Colors.black12,
                  //       //     radius: 40,
                  //       //     // backgroundImage: NetworkImage(imageUrl2),
                  //       //     child: CustomImage(storageUrl, 120, 120),
                  //       //   ),
                  //       // TextButton(
                  //       //     onPressed: () async {
                  //       //       await selectPhoto(context);
                  //       //       setState(() {
                  //       //         storageUrl =
                  //       //             'gs://korusell.appspot.com/avatars/' +
                  //       //                 data['uid'] +
                  //       //                 '.jpg';
                  //       //         ;
                  //       //       });
                  //       //     },
                  //       //     child: Text('Изменить фото')),

                  //       CustomCard1(user: dummyUser),
                  //       MainButton('🏷  Мои Объявления', (() => {})),

                  //       MainButton('💼  Мой Бизнес', (() => {})),
                  //       SizedBox(
                  //         height: 15,
                  //       ),
                  //       MainButton('👨🏻‍💻  Управление Аккаунтом', (() => {})),
                  //       MainButton('✉️  Написать Разработчикам', (() => {})),
                  //     ],
                  //   ),
                  // );
                },
              ),
              SubmitButton(
                color: Colors.red,
                padding: 16,
                text: 'Выйти',
                onPressed: () => showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: const Text('Выход'),
                    content: const Text(
                        'Вы действительно хотите выйти из Аккаунта?'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () => Navigator.pop(context, 'Отмена'),
                        child: const Text('Отмена'),
                      ),
                      TextButton(
                        onPressed: () async {
                          await _signOut();
                          Navigator.pop(context);
                        },
                        child: const Text(
                          'Выйти',
                          style: TextStyle(color: Colors.red),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void openMyService(String uid) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MyService(uid)));
  }

  Widget myBusinessButton(String uid) {
    String sufix = '';
    return StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('services')
            // .where('uid', isLessThanOrEqualTo: widget.uid)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            sufix = '';
          } else {
            final docs = snapshot.data!.docs
              ..removeWhere(
                  (element) => !element['uid'].toString().contains(uid));
            if (docs.isEmpty) {
              sufix = '';
            } else {
              String name = docs.first.get('name');
              if (name.length > 20) {
                sufix = name.substring(0, 20);
              } else {
                sufix = name;
              }
            }
          }
          return MainButton(
            '💼  Мой Бизнес',
            (() => Navigator.push(context,
                MaterialPageRoute(builder: (context) => MyService(uid)))),
            sufix: sufix,
          );
        });
  }

  Widget myAdvsButton(String uid) {
    String sufix = '';
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('adv')
          // .where('uid', isLessThanOrEqualTo: widget.uid)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) {
          sufix = '';
        } else {
          final docs = snapshot.data!.docs
            ..removeWhere(
                (element) => !element['uid'].toString().contains(uid));
          if (docs.isNotEmpty) {
            sufix = docs.length.toString();
          } else {
            sufix = '';
          }
        }
        return MainButton(
          '🏷  Мои Объявления',
          (() => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MyAdvList(
                        uid: uid,
                      )))),
          sufix: sufix,
        );
      },
    );
  }
}

class CustomCard1 extends StatelessWidget {
  const CustomCard1({
    Key? key,
    required this.user,
  }) : super(key: key);

  final u.User user;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: const EdgeInsets.fromLTRB(16, 10, 16, 10),
      child: ListTile(
        contentPadding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
        title: Column(
          children: [
            CustomRowForCard(field1: 'Эл. почта', field2: user.email),
            const Divider(
              height: 20,
            ),
            CustomRowForCard(field1: 'Имя', field2: user.name),
            const Divider(
              height: 20,
            ),
            CustomRowForCard(field1: 'Телефон', field2: user.phone),
          ],
        ),
      ),
    );
  }
}

class CustomRowForCard extends StatelessWidget {
  const CustomRowForCard({Key? key, required this.field1, required this.field2})
      : super(key: key);

  final String field1;
  final String field2;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
            child: Text(
          field1,
          style: const TextStyle(
            color: Colors.grey,
          ),
        )),
        Expanded(
          flex: 2,
          child: Text(
            field2,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        )
      ],
    );
  }
}
