import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:work_app/model/adv.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/profile/my_work_edit.dart';
import '../_elements/buttons.dart';
import '../work/work_details_view.dart';

class MyWork extends StatelessWidget {
  final Adv adv;

  const MyWork({Key? key, required this.adv}) : super(key: key);

  Future<Adv?> getMyAdv() async {
    // print('UID: ' + adv.uid);
    final doc = FirebaseFirestore.instance.collection('adv').doc(adv.uid);
    final snapshot = await doc.get();

    if (snapshot.exists) {
      // print('SNAPSHOT EXISTS!');
      // print(Adv.fromJson(snapshot.data()!));
      return Adv.fromJson(snapshot.data()!);
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Adv?>(
        future: getMyAdv(),
        builder: ((context, snapshot) {
          if (snapshot.hasData) {
            final adv = snapshot.data;
            return adv == null
                ? const Center(
                    child: Text('no adv'),
                  )
                : MyWorkView(adv: adv);
          } else {
            return const Center(
                // child: CircularProgressIndicator(),
                );
          }
        }));
  }
}

class MyWorkView extends StatelessWidget {
  final Adv adv;

  const MyWorkView({Key? key, required this.adv}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: 'Работа'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            WorkDetailsView(
              adv: adv,
            ),
            EditWorkView(adv: adv),
          ],
        ),
      ),
    );
  }
}

class EditWorkView extends StatelessWidget {
  final Adv adv;
  const EditWorkView({Key? key, required this.adv}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 40),
        Center(
          child: SubmitButton(
              text: 'Редактировать',
              onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => EditMyWork(adv)))),
        ),
        if (adv.isActive == '0.3')
          Center(
            child: SubmitButton(
                text: 'Восстановить Объявление',
                onPressed: () async {
                  await FirebaseFirestore.instance
                      .collection('adv')
                      .doc(adv.uid)
                      .update({'isActive': '1'});
                  Navigator.of(context).pop();
                  final snackbar = SnackBar(
                    content: const Text('Объявление восстановлено'),
                    action: SnackBarAction(
                      label: 'Отменить',
                      onPressed: () async {
                        await FirebaseFirestore.instance
                            .collection('adv')
                            .doc(adv.uid)
                            .update({'isActive': '0.3'});
                      },
                    ),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackbar);
                }),
          ),
        if (adv.isActive == '1')
          Center(
            child: SubmitButton(
                text: 'Скрыть объявление',
                onPressed: () async {
                  await FirebaseFirestore.instance
                      .collection('adv')
                      .doc(adv.uid)
                      .update({'isActive': '0.3'});
                  Navigator.of(context).pop();
                  final snackbar = SnackBar(
                    content: const Text('Объявление скрыто'),
                    action: SnackBarAction(
                      label: 'Отменить',
                      onPressed: () async {
                        await FirebaseFirestore.instance
                            .collection('adv')
                            .doc(adv.uid)
                            .update({'isActive': '1'});
                      },
                    ),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackbar);
                }),
          ),
        Center(
          child: SubmitButton(
              color: Colors.red,
              text: 'Удалить',
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          title: const Text(
                              'Вы уверены что хотите удалить Вакансию?'),
                          actions: [
                            TextButton(
                                onPressed: () async {
                                  Navigator.pop(context);
                                  FirebaseFirestore.instance
                                      .collection('adv')
                                      .doc(adv.uid)
                                      .delete();
                                },
                                child: const Text('Удалить')),
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: const Text('Отмена'))
                          ],
                        ));
                Navigator.pop(context);
              }),
        ),
        const SizedBox(height: 80),
      ],
    );
  }
}
