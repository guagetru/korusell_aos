import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';

class Chat extends StatelessWidget {
  const Chat({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: 'Обратная Связь'),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const _TextButton(
                'Telegram/@korusell_bot',
                'assets/images/social/telegram.png',
                'https://t.me/korusell_bot'),
            const Divider(),
            const _TextButton(
                'Instagram/@korusell',
                'assets/images/social/instagram.png',
                'https://www.instagram.com/korusell'),
            const Divider(),
            const _TextButton(
                'Почта/ceo@korusell.com',
                'assets/images/social/email_login.png',
                'mailto:ceo@korusell.com'),
            const Divider(),
            _TextButton('Сообщение', 'assets/images/social/sms.png',
                'sms:+821083872508?body=Сообщение от ${FirebaseAuth.instance.currentUser!.email} ::  '),
          ],
        ),
      ),
    );
  }
}

class _TextButton extends StatelessWidget {
  final String text;
  final String image;
  final String link;

  const _TextButton(this.text, this.image, this.link);

  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
        label: Text(
          text,
          style: const TextStyle(fontSize: 15),
        ),
        icon: SizedBox(height: 30, width: 30, child: Image.asset(image)),
        onPressed: () {
          launch(link);
        });
  }
}
