import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:work_app/model/adv.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import '../../utils/functions.dart';
import '../../utils/image_controller/carousel_adv.dart';
import '../_elements/business.dart';
import '../_elements/buttons.dart';
import '../_elements/owner.dart';
import 'my_adv_edit.dart';

class MyAdv extends StatelessWidget {
  final Adv adv;

  const MyAdv({Key? key, required this.adv}) : super(key: key);

  Future<Adv?> getMyAdv() async {
    // print('UID: ' + adv.uid);
    final doc = FirebaseFirestore.instance.collection('adv').doc(adv.uid);
    final snapshot = await doc.get();

    if (snapshot.exists) {
      return Adv.fromJson(snapshot.data()!);
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Adv?>(
        future: getMyAdv(),
        builder: ((context, snapshot) {
          if (snapshot.hasData) {
            final adv = snapshot.data;
            return adv == null
                ? const Center(
                    child: Text('no adv'),
                  )
                : MyAdvView(adv: adv);
          } else {
            return const Center(
                // child: CircularProgressIndicator(),
                );
          }
        }));
  }
}

// ignore: must_be_immutable
class MyAdvView extends StatelessWidget {
  final Adv adv;

  MyAdvView({Key? key, required this.adv}) : super(key: key);

  double currentIndexPage = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: adv.name),
      body: SingleChildScrollView(
        child: Column(
          children: [
            int.parse(adv.images) > 0
                ? CarouselAdv(adv.images, adv.uid, currentIndexPage)
                : const SizedBox(),
            Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Owner(adv.uid.substring(0, 28)),
                  Business(adv.uid.substring(0, 28)),
                  // TextButton.icon(
                  //   onPressed: () {
                  //     showModalBottomSheet(
                  //       useRootNavigator: false,
                  //       context: context,
                  //       builder: (BuildContext context) {
                  //         return ContactView(
                  //             adv.uid.substring(0, 28), avatarUrl);
                  //       },
                  //     );
                  //   },
                  //   icon: ClipRRect(
                  //       borderRadius: BorderRadius.circular(10),
                  //       child: CustomImage(avatarUrl, 20, 20, true)),
                  //   label: const Text("Контактное лицо"),
                  // ),
                  // TextButton.icon(
                  //   onPressed: () async {
                  //     late Service service;
                  //     await getService(adv.uid.substring(0, 28))
                  //         .then((value) => service = value!);
                  //     await Navigator.of(context).push(MaterialPageRoute(
                  //         builder: (context) => ServiceDetails(service)));
                  //   },
                  //   icon: ClipRRect(
                  //       borderRadius: BorderRadius.circular(10),
                  //       child: CustomImage(businessUrl, 20, 20, true)),
                  //   label: const Text("Бизнес"),
                  // ),
                  Text(
                    formattedDate(adv.createdAt),
                    style: const TextStyle(fontSize: 13, color: Colors.grey),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    adv.name,
                    maxLines: 2,
                    style: const TextStyle(
                        fontSize: 17, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [const Text('Город:  '), Text(adv.city)],
                  ),
                  const SizedBox(height: 10),
                  if (adv.price != '')
                    Text(
                      adv.price,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  const SizedBox(height: 10),
                  const Divider(
                    height: 10,
                    thickness: 1,
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    'Описание',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 5),
                  Text(adv.description),
                  const SizedBox(height: 10),
                  const Divider(
                    height: 10,
                    thickness: 1,
                  ),
                  const SizedBox(height: 20),
                ],
              ),
            ),
            if (adv.phone != "")
              Center(
                child: SubmitButton(
                    text: adv.phone,
                    onPressed: () => launch("tel://${adv.phone}")),
              ),
            const SizedBox(height: 40),
            Center(
              child: SubmitButton(
                  text: 'Редактировать',
                  onPressed: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => EditMyAdv(adv)))),
            ),
            if (adv.isActive == '0.3')
              Center(
                child: SubmitButton(
                    text: 'Восстановить Объявление',
                    onPressed: () async {
                      await FirebaseFirestore.instance
                          .collection('adv')
                          .doc(adv.uid)
                          .update({'isActive': '1'});
                      Navigator.of(context).pop();
                      final snackbar = SnackBar(
                        content: const Text('Объявление восстановлено'),
                        action: SnackBarAction(
                          label: 'Отменить',
                          onPressed: () async {
                            await FirebaseFirestore.instance
                                .collection('adv')
                                .doc(adv.uid)
                                .update({'isActive': '0.3'});
                          },
                        ),
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackbar);
                    }),
              ),
            if (adv.isActive == '1')
              Center(
                child: SubmitButton(
                    text: 'Скрыть объявление',
                    onPressed: () async {
                      await FirebaseFirestore.instance
                          .collection('adv')
                          .doc(adv.uid)
                          .update({'isActive': '0.3'});
                      Navigator.of(context).pop();
                      final snackbar = SnackBar(
                        content: const Text('Объявление скрыто'),
                        action: SnackBarAction(
                          label: 'Отменить',
                          onPressed: () async {
                            await FirebaseFirestore.instance
                                .collection('adv')
                                .doc(adv.uid)
                                .update({'isActive': '1'});
                          },
                        ),
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackbar);
                    }),
              ),
            Center(
              child: SubmitButton(
                  color: Colors.red,
                  text: 'Удалить',
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              title: const Text(
                                  'Вы уверены что хотите удалить объявление?'),
                              actions: [
                                TextButton(
                                    onPressed: () async {
                                      Navigator.pop(context);
                                      FirebaseFirestore.instance
                                          .collection('adv')
                                          .doc(adv.uid)
                                          .delete();

                                      for (var i = 0;
                                          i < int.parse(adv.images);
                                          i++) {
                                        String image = adv.uid +
                                            'ADV' +
                                            i.toString() +
                                            '.jpg';
                                        FirebaseStorage.instance
                                            .ref()
                                            .child('advImages')
                                            .child(image)
                                            .delete();
                                      }
                                      // ignore: avoid_print
                                      print(
                                          'ADV ${adv.uid} SUCCESSFULLY DELETED');

                                      // FirebaseStorage.instance
                                      //     .ref()
                                      //     .child('images')
                                      //     .child(adv.uid + '0')
                                      //     .delete();
                                    },
                                    child: const Text('Удалить')),
                                TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text('Отмена'))
                              ],
                            ));
                    Navigator.pop(context);
                  }),
            ),
            const SizedBox(height: 80),
          ],
        ),
      ),
    );
  }
}
