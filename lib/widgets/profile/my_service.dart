import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:work_app/model/service.dart';
import 'package:work_app/utils/image_controller/carousel.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/_elements/owner.dart';
import 'package:work_app/widgets/profile/my_service_edit.dart';
import 'package:work_app/widgets/profile/my_service_new.dart';

import '../_elements/buttons.dart';
import '../service/service_details_view.dart';

class MyService extends StatelessWidget {
  final String uid;
  const MyService(this.uid, {Key? key}) : super(key: key);

  Future<Service?> getMyService() async {
    // print('UID: ' + uid);
    final docUser = FirebaseFirestore.instance.collection('services').doc(uid);
    final snapshot = await docUser.get();

    if (snapshot.exists) {
      // print('SNAPSHOT EXISTS!');
      // print(Service.fromJson(snapshot.data()!));
      return Service.fromJson(snapshot.data()!);
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Service?>(
        future: getMyService(),
        builder: ((context, snapshot) {
          // print(snapshot.data);
          if (snapshot.hasData) {
            final service = snapshot.data;

            return service == null
                ? const Center(
                    child: Text('no user'),
                  )
                : MyServiceView(service);
          } else {
            return Center(
              child: AddNewService(uid),
            );
          }
        }));
  }
}

class MyServiceView extends StatefulWidget {
  final Service service;

  const MyServiceView(this.service, {Key? key}) : super(key: key);

  @override
  State<MyServiceView> createState() => _MyServiceViewState();
}

class _MyServiceViewState extends State<MyServiceView> {
  double currentIndexPage = 0.0;

  @override
  Widget build(BuildContext context) {
    void goToEdit() {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => EditMyService(widget.service)));
    }

    return Scaffold(
      appBar: MyAppBar(label: widget.service.name),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            int.parse(widget.service.images) > 0
                ? Carousel(widget.service.images, currentIndexPage)
                : const SizedBox(),

            Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (!widget.service.uid.contains('aaaaaaaaaaaaaaaaaaaaaaaa'))
                    Owner(widget.service.uid),
                  // TextButton.icon(
                  //   onPressed: () {
                  //     showModalBottomSheet(
                  //       useRootNavigator: false,
                  //       context: context,
                  //       builder: (BuildContext context) {
                  //         return ContactView(widget.service.uid, avatarUrl);
                  //       },
                  //     );
                  //   },
                  //   icon: ClipRRect(
                  //       borderRadius: BorderRadius.circular(10),
                  //       child: CustomImage(avatarUrl, 20, 20, true)),
                  //   label: const Text("Контактное лицо"),
                  // ),
                  if (widget.service.address != "")
                    Row(
                      children: [
                        const Text('Адрес:  '),
                        Flexible(
                          child: Text(
                            widget.service.address,
                            softWrap: false,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        ),
                        IconButton(
                            onPressed: () {
                              Clipboard.setData(
                                  ClipboardData(text: widget.service.address));
                              final snackbar = SnackBar(
                                content: Text(widget.service.address),
                                action: SnackBarAction(
                                  label: 'Адрес скопирован',
                                  onPressed: () {
                                    // Some code to undo the change.
                                  },
                                ),
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackbar);
                            },
                            icon: const Icon(
                              Icons.copy,
                              size: 15,
                              color: Colors.blue,
                            )),
                      ],
                    ),
                  const SizedBox(height: 10),
                  Text(
                    widget.service.name,
                    maxLines: 2,
                    style: const TextStyle(
                        fontSize: 17, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      const Text('Город:  '),
                      Text(widget.service.city)
                    ],
                  ),
                  const SizedBox(height: 10),
                  const Divider(
                    height: 10,
                    thickness: 1,
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    'Описание',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 5),
                  Text(widget.service.description),
                  const SizedBox(height: 10),
                  const Divider(
                    height: 10,
                    thickness: 1,
                  ),
                  const SizedBox(height: 20),
                ],
              ),
            ),
            if (widget.service.phone != "")
              Center(
                child: SubmitButton(
                    text: widget.service.phone,
                    onPressed: () => launch("tel://${widget.service.phone}")),
              ),
            const SizedBox(height: 20),
            SocialWidget(widget.service),

            // ServiceDetailsView(
            //   service: service,
            // ),
            Center(
              child: SubmitButton(
                  text: 'Редактировать', onPressed: () => goToEdit()),
            ),
            // Center(
            //   child: SubmitButton(
            //       text: 'Восстановить Объявление', onPressed: () {}),
            // ),
            Center(
              child: SubmitButton(
                  color: Colors.red,
                  text: 'Удалить',
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              title: const Text(
                                  'Вы уверены что хотите удалить Ваш бизнес?'),
                              actions: [
                                TextButton(
                                    onPressed: () async {
                                      await FirebaseFirestore.instance
                                          .collection('services')
                                          .doc(widget.service.uid)
                                          .delete();

                                      for (var i = 0;
                                          i < int.parse(widget.service.images);
                                          i++) {
                                        String image = widget.service.uid +
                                            i.toString() +
                                            '.jpg';
                                        FirebaseStorage.instance
                                            .ref()
                                            .child('images')
                                            .child(image)
                                            .delete();
                                      }
                                      // ignore: avoid_print
                                      print(
                                          'SERVICE ${widget.service.uid} SUCCESSFULLY DELETED');
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text('Удалить')),
                                TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text('Отмена'))
                              ],
                            ));
                  }),
            ),
            const SizedBox(height: 80),
          ],
        ),
      ),
    );
  }
}
