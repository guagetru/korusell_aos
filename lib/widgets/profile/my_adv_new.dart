import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:work_app/utils/constants.dart';
import 'package:work_app/utils/image_controller/adv_image.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import '../../utils/functions.dart';
import '../_elements/buttons.dart';

class AddNewAdv extends StatefulWidget {
  final String uid;

  const AddNewAdv(this.uid, {Key? key}) : super(key: key);

  @override
  State<AddNewAdv> createState() => _AddNewAdvState();
}

class _AddNewAdvState extends State<AddNewAdv> {
  final _formKey = GlobalKey<FormState>();
  String uid = '';
  bool loading = false;
  bool isVisible = false;
  bool showLinks = false;

  String id = '';
  String name = '';
  String category = 'transport';
  String subcategory = '';
  String city = 'Ансан';
  String otherCity = '';
  String description = '';
  String phone = '';
  String images = '0';
  String isActive = '';

  String updatedAt = '';
  String createdAt = '';

  String gender = '';
  String price = '';
  String shift = '';
  List<dynamic> age = ['', ''];
  List<dynamic> visa = ['', ''];

  @override
  void initState() {
    uid = widget.uid + dateforAdv();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: 'Добавить Объявление'),
      body: Form(
          key: _formKey,
          child: ListView(
            children: [
              const SizedBox(
                height: 15,
              ),
              // UserImage(onFileChanged: (imageUrl) {
              //   setState(() {
              //     this.imageUrl = imageUrl;
              //   });
              // }),
              AdvImage(int.parse(images), uid),
              const SizedBox(height: 15),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      validator: _requiredValidator,
                      decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 13),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isCollapsed: true,
                        label: Text('Заголовок *'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => name = value,
                    ),
                    Row(
                      children: [
                        const Text('Город *:  '),
                        DropdownButton(
                          underline: const Text(''),
                          value: city,
                          // icon: const Icon(Icons.keyboard_arrow_down),
                          items: citiesN.map((String items) {
                            return DropdownMenuItem(
                              child: Text(
                                items,
                                style: const TextStyle(fontSize: 14),
                              ),
                              value: items,
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              city = newValue!;
                              if (city == "Другой Город") {
                                isVisible = true;
                              } else {
                                isVisible = false;
                              }
                            });
                          },
                        ),
                      ],
                    ),
                    Visibility(
                      visible: isVisible,
                      child: TextFormField(
                        validator: _requiredValidator,
                        decoration: const InputDecoration(
                          labelStyle: TextStyle(fontSize: 13),
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                          isCollapsed: true,
                          label: Text('Название Города *'),
                          border: OutlineInputBorder(),
                        ),
                        onChanged: (value) => otherCity = value,
                      ),
                    ),
                    // const SizedBox(height: 15),
                    Row(
                      children: [
                        const Text('Категории *: '),
                        DropdownButton(
                          value: category,
                          underline: const Text(''),
                          items: advCategoriesN.map((String items) {
                            return DropdownMenuItem(
                              child: Text(
                                advNamesN[items]!,
                                style: const TextStyle(fontSize: 14),
                                overflow: TextOverflow.ellipsis,
                              ),
                              value: items,
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              category = newValue!;
                            });
                          },
                        ),
                      ],
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      validator: _requiredValidator,
                      keyboardType: TextInputType.phone,
                      decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 13),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isCollapsed: true,
                        label: Text('Номер Телефона *'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => phone = value,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 13),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isCollapsed: true,
                        label: Text('Цена'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => price = value,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      maxLines: 10,
                      minLines: 1,
                      decoration: const InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 5),
                        isCollapsed: true,
                        labelStyle: TextStyle(fontSize: 13),
                        label: Text('Описание'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => description = value,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    if (loading) ...[
                      const Center(child: CircularProgressIndicator()),
                    ],
                  ],
                ),
              ),
              if (!loading) ...[
                SubmitButton(
                  text: 'Создать Объявление',
                  onPressed: () {
                    if (_formKey.currentState != null &&
                        _formKey.currentState!.validate()) {
                      _createService(uid);
                    }
                  },
                  // padding: 16,
                ),
                const SizedBox(
                  height: 20,
                ),
              ]
            ],
          )),
    );
  }

  String? _requiredValidator(String? text) {
    if (text == null || text.trim().isEmpty) {
      return 'Пожалуйста, заполните это поле';
    }
    return null;
  }

  Future _createService(String uid) async {
    setState(() {
      loading = true;
      if (city != 'Другой Город') {
        otherCity = city;
      }
    });

    images = advImagesCount.toString();
    // deleteImages(serviceImagesCount);
    if (advImagesCount == 1) {
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV1.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV2.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV3.jpg')
          .delete();
    } else if (advImagesCount == 2) {
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV2.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV3.jpg')
          .delete();
    } else if (advImagesCount == 3) {
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV3.jpg')
          .delete();
    } else if (advImagesCount == 0) {
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV0.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV1.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV2.jpg')
          .delete();
      FirebaseStorage.instance
          .refFromURL('gs://korusell.appspot.com/advImages/${uid}ADV3.jpg')
          .delete();
    }

    try {
      // var locations = await locationFromAddress(address);
      var date = dateForCreateAdv();
      await FirebaseFirestore.instance.collection('adv').doc(uid).set({
        'id': uid,
        'uid': uid,
        'price': price,
        'category': category,
        'subcategory': subcategory,
        'city': otherCity,
        'description': description,
        'name': name,
        'phone': phone,
        'createdAt': date,
        'updatedAt': date,
        'isActive': "1",
        'images': images,
        'age': age,
        'gender': gender,
        'shift': shift,
        'visa': visa,
      });

      await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Объявление добавлено!'),
                // content: Text('Теперь Вы можете войти'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('OK')),
                ],
              ));
      Navigator.of(context).pop();
    } on FirebaseAuthException {
      // _handleSignUpError(e);
      setState(() {
        loading = false;
      });
    }
  }
}
