import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:work_app/utils/constants.dart';
import '../../model/adv.dart';
import '../../utils/functions.dart';
import '../_elements/app_bar.dart';
import '../_elements/buttons.dart';

class EditMyWork extends StatefulWidget {
  final Adv adv;

  const EditMyWork(this.adv, {Key? key}) : super(key: key);

  @override
  State<EditMyWork> createState() => _EditMyWorkState();
}

class _EditMyWorkState extends State<EditMyWork> {
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    name = widget.adv.name;
    price = widget.adv.price;
    description = widget.adv.description;
    phone = widget.adv.phone;
    subcategory = widget.adv.subcategory;

    if (citiesN.contains(widget.adv.city)) {
      city = widget.adv.city;
    } else {
      city = 'Другой Город';
      otherCity = widget.adv.city;
      isVisible = true;
    }

    if (widget.adv.visa.contains('F1')) {
      f1Checked = true;
    }
    if (widget.adv.visa.contains('F4')) {
      f4Checked = true;
    }
    if (widget.adv.visa.contains('H2')) {
      h2Checked = true;
    }
    if (widget.adv.visa.contains('Другая')) {
      otherVisaChecked = true;
    }
    if (widget.adv.visa.contains('Любая')) {
      allVisaChecked = true;
      f1Checked = true;
      otherVisaChecked = true;
      h2Checked = true;
      f4Checked = true;
    }

    if (widget.adv.gender.contains('👱🏼‍♂️')) {
      manChecked = true;
    }
    if (widget.adv.gender.contains('👩🏻')) {
      womanChecked = true;
    }

    if (widget.adv.shift.contains('🌞')) {
      dayChecked = true;
    }
    if (widget.adv.shift.contains('🌚')) {
      nightChecked = true;
    }
    if (widget.adv.age[0] == '20' && widget.adv.age[1] == '70') {
      anyAgeCheck = true;
      ageF = widget.adv.age[0];
      ageT = widget.adv.age[1];
    } else {
      anyAgeCheck = false;
      ageF = widget.adv.age[0];
      ageT = widget.adv.age[1];
    }

    super.initState();
  }

  bool loading = false;
  bool isVisible = false;
  bool showLinks = false;

  String id = '';
  String name = '';
  String category = '';
  String subcategory = 'factory';
  String city = 'Ансан';
  String otherCity = '';
  String description = '';
  String phone = '';
  String images = '';
  String isActive = '';

  String updatedAt = '';
  String createdAt = '';

  String gender = '';
  String price = '';
  String shift = '';
  List<dynamic> age = ['', ''];
  List<dynamic> visa = ['', ''];

  bool manChecked = false;
  bool womanChecked = false;
  bool dayChecked = false;
  bool nightChecked = false;
  bool allVisaChecked = false;
  bool h2Checked = false;
  bool f4Checked = false;
  bool f1Checked = false;
  bool otherVisaChecked = false;
  bool anyAgeCheck = false;
  String ageF = '20';
  String ageT = '60';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: 'Редактировать Вакансию'),
      body: Form(
          key: _formKey,
          child: ListView(
            children: [
              const SizedBox(
                height: 15,
              ),
              // UserImage(onFileChanged: (imageUrl) {
              //   setState(() {
              //     this.imageUrl = imageUrl;
              //   });
              // }),

              const SizedBox(height: 15),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      initialValue: widget.adv.name,
                      validator: _requiredValidator,
                      decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 13),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isCollapsed: true,
                        label: Text('Заголовок *'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => name = value,
                    ),
                    Row(
                      children: [
                        const Text('Город:  '),
                        DropdownButton(
                          underline: const Text(''),
                          value: city,
                          // icon: const Icon(Icons.keyboard_arrow_down),
                          items: citiesN.map((String items) {
                            return DropdownMenuItem(
                              child: Text(
                                items,
                                style: const TextStyle(fontSize: 14),
                              ),
                              value: items,
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              city = newValue!;
                              if (city == "Другой Город") {
                                isVisible = true;
                              } else {
                                isVisible = false;
                              }
                            });
                          },
                        ),
                      ],
                    ),
                    Visibility(
                      visible: isVisible,
                      child: TextFormField(
                        initialValue: otherCity,
                        validator: _requiredValidator,
                        decoration: const InputDecoration(
                          labelStyle: TextStyle(fontSize: 13),
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                          isCollapsed: true,
                          label: Text('Название Города *'),
                          border: OutlineInputBorder(),
                        ),
                        onChanged: (value) => otherCity = value,
                      ),
                    ),
                    // const SizedBox(height: 15),
                    Row(
                      children: [
                        const Text('Категории: '),
                        DropdownButton(
                          value: subcategory,
                          underline: const Text(''),
                          items: subcategories.map((String items) {
                            return DropdownMenuItem(
                              child: Text(
                                workIcons[items]![1],
                                style: const TextStyle(fontSize: 14),
                                overflow: TextOverflow.ellipsis,
                              ),
                              value: items,
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              subcategory = newValue!;
                            });
                          },
                        ),
                      ],
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      initialValue: widget.adv.phone,
                      validator: _requiredValidator,
                      keyboardType: TextInputType.phone,
                      decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 13),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isCollapsed: true,
                        label: Text('Номер Телефона *'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => phone = value,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      initialValue: widget.adv.price,
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 13),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isCollapsed: true,
                        label: Text('Зарплата'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => price = value,
                    ),
                    const SizedBox(height: 15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Пол:'),
                            Row(
                              children: [
                                const Text(
                                  '👱🏼‍♂️',
                                  style: TextStyle(fontSize: 20),
                                ),
                                Checkbox(
                                  shape: const CircleBorder(),
                                  value: manChecked,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      manChecked = value!;
                                    });
                                  },
                                ),
                                const Text(
                                  '👩🏻',
                                  style: TextStyle(fontSize: 20),
                                ),
                                Checkbox(
                                  shape: const CircleBorder(),
                                  value: womanChecked,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      womanChecked = value!;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Смена:'),
                            Row(
                              children: [
                                const Text(
                                  '🌞',
                                  style: TextStyle(fontSize: 20),
                                ),
                                Checkbox(
                                  shape: const CircleBorder(),
                                  value: dayChecked,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      dayChecked = value!;
                                    });
                                  },
                                ),
                                const Text(
                                  '🌚',
                                  style: TextStyle(fontSize: 20),
                                ),
                                Checkbox(
                                  shape: const CircleBorder(),
                                  value: nightChecked,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      nightChecked = value!;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    const Divider(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            const Text('Визы:'),
                            const SizedBox(
                              width: 20,
                            ),
                            const Text('Любая'),
                            Checkbox(
                              shape: const CircleBorder(),
                              value: allVisaChecked,
                              onChanged: (bool? value) {
                                setState(() {
                                  f1Checked = value!;
                                  f4Checked = value;
                                  h2Checked = value;
                                  otherVisaChecked = value;
                                  allVisaChecked = value;
                                });
                              },
                            ),
                          ],
                        ),
                        Visibility(
                          visible: !allVisaChecked,
                          child: Row(
                            children: [
                              const Text(
                                'H2',
                                style: TextStyle(fontSize: 15),
                              ),
                              Checkbox(
                                shape: const CircleBorder(),
                                value: h2Checked,
                                onChanged: (bool? value) {
                                  setState(() {
                                    h2Checked = value!;
                                  });
                                },
                              ),
                              const Text(
                                'F4',
                                style: TextStyle(fontSize: 15),
                              ),
                              Checkbox(
                                shape: const CircleBorder(),
                                value: f4Checked,
                                onChanged: (bool? value) {
                                  setState(() {
                                    f4Checked = value!;
                                  });
                                },
                              ),
                              const Text(
                                'F1',
                                style: TextStyle(fontSize: 15),
                              ),
                              Checkbox(
                                shape: const CircleBorder(),
                                value: f1Checked,
                                onChanged: (bool? value) {
                                  setState(() {
                                    f1Checked = value!;
                                  });
                                },
                              ),
                              const Text(
                                'Другие',
                                style: TextStyle(fontSize: 15),
                              ),
                              Checkbox(
                                shape: const CircleBorder(),
                                value: otherVisaChecked,
                                onChanged: (bool? value) {
                                  setState(() {
                                    otherVisaChecked = value!;
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const Divider(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            const Text('Возраст:'),
                            const SizedBox(
                              width: 20,
                            ),
                            const Text('Любой'),
                            Checkbox(
                              shape: const CircleBorder(),
                              value: anyAgeCheck,
                              onChanged: (bool? value) {
                                setState(() {
                                  anyAgeCheck = value!;
                                });
                              },
                            ),
                          ],
                        ),
                        Visibility(
                          visible: !anyAgeCheck,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              const Text(
                                'От',
                                style: TextStyle(fontSize: 15),
                              ),
                              DropdownButton(
                                underline: const Text(''),
                                value: ageF,
                                // icon: const Icon(Icons.keyboard_arrow_down),
                                items: ageFrom.map((String items) {
                                  return DropdownMenuItem(
                                    child: Text(
                                      items,
                                      style: const TextStyle(fontSize: 14),
                                    ),
                                    value: items,
                                  );
                                }).toList(),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    ageF = newValue!;
                                    // if (ageF == "Другой Город") {
                                    //   isVisible = true;
                                    // } else {
                                    //   isVisible = false;
                                    // }
                                  });
                                },
                              ),
                              const SizedBox(
                                width: 0,
                              ),
                              const Text(
                                'До',
                                style: TextStyle(fontSize: 15),
                              ),
                              DropdownButton(
                                underline: const Text(''),
                                value: ageT,
                                // icon: const Icon(Icons.keyboard_arrow_down),
                                items: ageTo.map((String items) {
                                  return DropdownMenuItem(
                                    child: Text(
                                      items,
                                      style: const TextStyle(fontSize: 14),
                                    ),
                                    value: items,
                                  );
                                }).toList(),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    ageT = newValue!;
                                    // if (ageF == "Другой Город") {
                                    //   isVisible = true;
                                    // } else {
                                    //   isVisible = false;
                                    // }
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const Divider(),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      initialValue: widget.adv.description,
                      maxLines: 10,
                      minLines: 1,
                      decoration: const InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 5),
                        isCollapsed: true,
                        labelStyle: TextStyle(fontSize: 13),
                        label: Text('Описание'),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) => description = value,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    if (loading) ...[
                      const Center(child: CircularProgressIndicator()),
                    ],
                    if (!loading) ...[
                      SubmitButton(
                        text: 'Обновить',
                        onPressed: () {
                          if (_formKey.currentState != null &&
                              _formKey.currentState!.validate()) {
                            _createService();
                          }
                        },
                        // padding: 16,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ]
                  ],
                ),
              ),
            ],
          )),
    );
  }

  String? _requiredValidator(String? text) {
    if (text == null || text.trim().isEmpty) {
      return 'Пожалуйста, заполните это поле';
    }
    return null;
  }

  Future _createService() async {
    setState(() {
      loading = true;
      if (city != 'Другой Город') {
        otherCity = city;
      }
    });

    visa = [];
    if (allVisaChecked) {
      visa = ['Любая'];
    } else {
      if (h2Checked) {
        visa.add('H2');
      }
      if (f4Checked) {
        visa.add('F4');
      }
      if (f1Checked) {
        visa.add('F1');
      }
      if (otherVisaChecked) {
        visa.add('Другая');
      }
    }

    if (manChecked) {
      gender = '👱🏼‍♂️';
    }
    if (womanChecked) {
      gender = '👩🏻';
    }
    if (womanChecked && manChecked) {
      gender = '👱🏼‍♂️👩🏻';
    }

    if (dayChecked) {
      shift = '🌞';
    }
    if (nightChecked) {
      shift = '🌚';
    }
    if (dayChecked && nightChecked) {
      shift = '🌞🌚';
    }

    age = [ageF, ageT];
    var date = dateForCreateAdv();
    try {
      await FirebaseFirestore.instance
          .collection('adv')
          .doc(widget.adv.uid)
          .set({
        'id': widget.adv.id,
        'uid': widget.adv.uid,
        'price': price,
        'category': 'work',
        'subcategory': subcategory,
        'city': otherCity,
        'description': description,
        'name': name,
        'phone': phone,
        'createdAt': widget.adv.createdAt,
        'updatedAt': date,
        'isActive': "1",
        'images': '0',
        'age': age,
        'gender': gender,
        'shift': shift,
        'visa': visa,
      });

      await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: const Text('Вакансия обновлена!'),
                // content: Text('Теперь Вы можете войти'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('OK')),
                ],
              ));
      Navigator.of(context).pop();
    } on FirebaseAuthException {
      // _handleSignUpError(e);
      setState(() {
        loading = false;
      });
    }
  }
}
