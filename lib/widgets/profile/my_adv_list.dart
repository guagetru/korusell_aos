import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:work_app/model/adv.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/advertisement/adv_view2.dart';
import 'package:work_app/widgets/profile/my_adv.dart';
import 'package:work_app/widgets/profile/my_adv_new.dart';
import 'package:work_app/widgets/profile/my_work.dart';
import 'package:work_app/widgets/profile/my_work_new.dart';
import '../work/work_list_view.dart';

class MyAdvList extends StatefulWidget {
  final String uid;

  const MyAdvList({required this.uid, Key? key}) : super(key: key);

  @override
  _MyAdvListState createState() => _MyAdvListState();
}

class _MyAdvListState extends State<MyAdvList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: MyAppBar(
        label: 'Мои Объявления',
        actions: [
          PopupMenuButton(
            icon: const Icon(
              Icons.add,
              color: Colors.black,
            ),
            onSelected: (value) {
              if (value == 0) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddNewAdv(widget.uid)));
              } else if (value == 1) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddNewWork(widget.uid)));
              }
            },
            itemBuilder: (BuildContext bc) {
              return const [
                PopupMenuItem(
                  child: Text(
                    "Добавить Объявление",
                    style: TextStyle(color: Colors.black, fontSize: 14),
                  ),
                  value: 0,
                ),
                PopupMenuItem(
                  child: Text(
                    "Добавить Вакансию",
                    style: TextStyle(color: Colors.black, fontSize: 14),
                  ),
                  value: 1,
                ),
              ];
            },
          )
        ],
      ),
      body: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('adv')
              // .where('uid', isLessThanOrEqualTo: widget.uid)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return const Text('... Загрузка');
            } else {
              final docs = snapshot.data!.docs
                ..removeWhere((element) =>
                    !element['uid'].toString().contains(widget.uid));
              // where((element) =>
              // element['uid'].toString().contains(widget.uid));
              if (docs.isNotEmpty) {
                return ListView.separated(
                  itemCount: docs.length,
                  itemBuilder: (BuildContext context, int index) {
                    final QueryDocumentSnapshot<Object?>? data = docs[index];
                    Adv adv = Adv(
                        age: data?.get('age'),
                        category: data?.get('category'),
                        city: data?.get('city'),
                        createdAt: data?.get('createdAt'),
                        description: data?.get('description'),
                        gender: data?.get('gender'),
                        id: data?.get('id'),
                        images: data?.get('images'),
                        isActive: data?.get('isActive'),
                        name: data?.get('name'),
                        phone: data?.get('phone'),
                        price: data?.get('price'),
                        shift: data?.get('shift'),
                        subcategory: data?.get('subcategory'),
                        uid: data?.get('uid'),
                        updatedAt: data?.get('updatedAt'),
                        visa: data?.get('visa'));
                    String imageUrl = 'gs://korusell.appspot.com/advImages/' +
                        adv.uid +
                        'ADV' +
                        '0' +
                        '.jpg';
                    if (adv.category != 'work') {
                      return AdvView2(
                        cache: false,
                        adv: adv,
                        imageUrl: imageUrl,
                        widget: MyAdv(adv: adv),
                      );
                    } else {
                      return WorkView(
                        adv: adv,
                        widget: MyWorkView(adv: adv),
                      );
                    }
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const Divider();
                  },
                );
              } else {
                return const Center(
                  heightFactor: 2,
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      'Нажмите на "+" чтобы добавить Объявление или Вакансию',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                );
              }
            }
          }),
    );
  }
}
