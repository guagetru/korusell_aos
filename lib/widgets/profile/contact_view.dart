import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:work_app/widgets/profile/profile.dart';

import '../_elements/buttons.dart';
import '../../model/user.dart' as u;

class ContactView extends StatefulWidget {
  final String uid;
  final String avatarUrl;

  const ContactView(this.uid, this.avatarUrl, {Key? key}) : super(key: key);

  @override
  State<ContactView> createState() => _ContactViewState();
}

class _ContactViewState extends State<ContactView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('ContactView!'),
      // ),
      body: FutureBuilder<DocumentSnapshot>(
          future: FirebaseFirestore.instance
              .collection('users')
              .doc(widget.uid)
              .get(),
          builder:
              (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (snapshot.hasError) {
              return const Text("... Загрузка");
            }

            if (snapshot.hasData && !snapshot.data!.exists) {
              return const Text("Пользователь удален");
            }

            if (snapshot.connectionState == ConnectionState.done) {
              // print(snapshot.connectionState);
              Map<String, dynamic> data =
                  snapshot.data!.data() as Map<String, dynamic>;
              // print(data);
              u.User user = u.User.fromJson(data);
              Image image = Image.network(data['imageUrl'] ?? '');
              if (user.imageUrl == '') {
                image = Image(
                  image: FirebaseImage(widget.avatarUrl,
                      shouldCache: true,
                      cacheRefreshStrategy:
                          CacheRefreshStrategy.BY_METADATA_DATE),
                );
              }

              return SizedBox(
                child: Column(
                  children: [
                    const Padding(padding: EdgeInsets.all(10)),
                    Container(
                        width: 80,
                        height: 80,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.cover, image: image.image))),
                    Card(
                        elevation: 0,
                        margin: const EdgeInsets.fromLTRB(16, 10, 16, 10),
                        child: ListTile(
                          contentPadding:
                              const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          title: Column(children: [
                            CustomRowForCard(
                                field1: 'Эл. почта', field2: user.email),
                            const Divider(
                              height: 20,
                            ),
                            CustomRowForCard(field1: 'Имя', field2: user.name),
                          ]),
                        )),
                    const SizedBox(height: 20),
                    if (data['phone'] != '')
                      Center(
                        child: SubmitButton(
                            text: data['phone'],
                            onPressed: () => launch("tel://${data['phone']}")),
                      ),
                  ],
                ),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }
}
