import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:work_app/model/service.dart';
import '../../utils/image_controller/custom_image.dart';
import '../service/service_details.dart';

class MapSheet extends StatelessWidget {
  final Service service;

  const MapSheet(this.service, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String imageUrl =
        'gs://korusell.appspot.com/images/' + service.uid + '0' + '.jpg';
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.27,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomImage(imageUrl, MediaQuery.of(context).size.width * 0.25,
                  MediaQuery.of(context).size.width * 0.25, true),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width * 0.70,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      service.name,
                      maxLines: 1,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 15),
                    ),
                    if (service.address != "")
                      Row(
                        children: [
                          const Text(
                            'Адрес:  ',
                            style: TextStyle(fontSize: 13),
                          ),
                          Flexible(
                            child: Text(
                              service.address,
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: const TextStyle(fontSize: 13),
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                Clipboard.setData(
                                    ClipboardData(text: service.address));
                                final snackbar = SnackBar(
                                  content: Text(service.address),
                                  action: SnackBarAction(
                                    label: 'Адрес скопирован',
                                    onPressed: () {
                                      // Some code to undo the change.
                                    },
                                  ),
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackbar);
                              },
                              icon: const Icon(
                                Icons.copy,
                                size: 15,
                                color: Colors.blue,
                              )),
                        ],
                      ),
                    Text(
                      service.description,
                      style: const TextStyle(fontSize: 12),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    if (service.phone != "")
                      TextButton(
                          child: Text(
                            service.phone,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          onPressed: () => launch("tel://${service.phone}")),
                  ],
                ),
              ),
            ],
          ),
          onTap: () => {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ServiceDetails(service))),
          },
        ),
      ),
    );
  }
}
