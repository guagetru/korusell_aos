import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:work_app/model/service.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/map/map_filter.dart';
import './map_sheet.dart';
import '../../utils/constants.dart';

const double mainZoom = 7;

class MapView extends StatefulWidget {
  final Position? position;
  const MapView(this.position, {Key? key}) : super(key: key);
  // const MapView({Key? key}) : super(key: key);

  @override
  State<MapView> createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  late GoogleMapController _googleMapController;
  String category = "all";
  String city = "Все Города";
  String categoryName = "Все";
  callback(newCategory, newCity) {
    setState(() {
      // markers = {};
      // print(newCity);
      category = newCategory;
      city = newCity;
    });
  }

  Set<Marker> markers = {};

  late BitmapDescriptor embassy;
  late Map<String, BitmapDescriptor> pins;

  @override
  void initState() {
    super.initState();
    setCustomMarker();
  }

  void setCustomMarker() async {
    pins = {
      'embassy': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/embassy.png'),
      'food': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/food.png'),
      'shop': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/shop.png'),
      'cars': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/cars.png'),
      'connect': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/connect.png'),
      'study': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/study.png'),
      'party': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/party.png'),
      'docs': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/docs.png'),
      'health': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/health.png'),
      'beauty': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/beauty.png'),
      'insurance': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/insurance.png'),
      'transport': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/transport.png'),
      'travel': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/travel.png'),
      'workshop': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/workshop.png'),
      'other': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/other.png'),
      'products': await BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/images/map/products.png'),
    };

    embassy = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: 2.5),
        'assets/images/map/embassy.png');
  }

  List<LatLng> coordinates = [
    const LatLng(36.62257816899407, 127.91520089316795),
    const LatLng(37.31639679242606, 126.8256217710536),
    const LatLng(37.16834087290789, 126.801294705907),
    const LatLng(37.45771638152154, 126.7028438251576),
    const LatLng(37.52146229568448, 126.98610893732737),
    const LatLng(37.28338391588353, 127.01187706655084),
    const LatLng(36.7818299238274, 127.00476323050401),
    const LatLng(36.80244428186357, 127.18186756201197),
    const LatLng(36.63926314157214, 127.47918258581026),
    const LatLng(37.0104484860411, 126.97948650235675),
    const LatLng(36.78116367382427, 126.45389687374652),
    const LatLng(36.927903013124414, 127.04292717110127),
  ];

  void moveCamera(String city) async {
    // print(city);
    const double zoom = 11;
    if (city == 'Ансан') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[1], zoom));
    }
    if (city == 'Хвасонг') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[2], zoom));
    }
    if (city == 'Инчхон') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[3], zoom));
    }
    if (city == 'Сеул') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[4], zoom));
    }
    if (city == 'Сувон') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[5], zoom));
    }
    if (city == 'Асан') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[6], zoom));
    }
    if (city == 'Чхонан') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[7], zoom));
    }
    if (city == 'Чхонджу') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[8], zoom));
    }
    if (city == 'Пхёнтхэк') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[9], zoom));
    }
    if (city == 'Сосан') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[10], zoom));
    }
    if (city == 'Дунпо') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[11], zoom));
    }
    if (city == 'Другой Город' || city == 'Все Города') {
      await _googleMapController
          .animateCamera(CameraUpdate.newLatLngZoom(coordinates[0], mainZoom));
    }
  }

  @override
  Widget build(BuildContext context1) {
    final allAdvallCity =
        FirebaseFirestore.instance.collection('services').snapshots();
    final allAdvsomeCity = FirebaseFirestore.instance
        .collection('services')
        .where('city', isEqualTo: city)
        .snapshots();
    final someAdvallCity = FirebaseFirestore.instance
        .collection('services')
        .where('category', isEqualTo: category)
        .snapshots();
    final someAdvsomeCity = FirebaseFirestore.instance
        .collection('services')
        .where('city', isEqualTo: city)
        .where('category', isEqualTo: category)
        .snapshots();
    final allAdvotherCity = FirebaseFirestore.instance
        .collection('services')
        // .where('city', whereNotIn: DBcities)
        .snapshots();
    final someAdvotherCity = FirebaseFirestore.instance
        .collection('services')
        // .where('city', whereNotIn: DBcities)
        .orderBy('city')
        .where('category', isEqualTo: category)
        .snapshots();

    var reference = someAdvsomeCity;
    if (category == "all") {
      if (city == "Другой Город") {
        reference = allAdvotherCity;
      } else if (city == "Все Города") {
        reference = allAdvallCity;
      } else {
        reference = allAdvsomeCity;
      }
    } else if (category != "all") {
      if (city == "Другой Город") {
        reference = someAdvotherCity;
      } else if (city == "Все Города") {
        reference = someAdvallCity;
      }
    }

    return Scaffold(
      appBar: MyAppBar(
        label: 'Карта',
        actions: [
          IconButton(
            color: Colors.black,
            onPressed: () async {
              await showModalBottomSheet(
                useRootNavigator: false,
                context: context1,
                builder: (BuildContext context) {
                  return MapFilter(category, city, callback);
                },
              );
              moveCamera(city);
            },
            icon: const Icon(CupertinoIcons.line_horizontal_3_decrease),
          ),
        ],
      ),
      body: SafeArea(
        child: StreamBuilder(
          stream: reference,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              markers = {};
              var docs = snapshot.data!.docs;
              if (city == 'Другой Город') {
                docs = snapshot.data!.docs
                  ..removeWhere((element) =>
                      element['city'] == 'Ансан' ||
                      element['city'] == 'Хвасонг' ||
                      element['city'] == 'Инчхон' ||
                      element['city'] == 'Сеул' ||
                      element['city'] == 'Сувон' ||
                      element['city'] == 'Асан' ||
                      element['city'] == 'Чхонан' ||
                      element['city'] == 'Чхонджу' ||
                      element['city'] == 'Пхёнтхэк' ||
                      element['city'] == 'Сосан' ||
                      element['city'] == 'Дунпо');
              }
              for (var item in docs) {
                Service service = Service(
                    // id: item['id'],
                    address: item['address'],
                    category: item['category'],
                    city: item['city'],
                    description: item['description'],
                    images: item['images'],
                    latitude: item['latitude'],
                    longitude: item['longitude'],
                    name: item['name'],
                    phone: item['phone'],
                    social: item['social'],
                    uid: item['uid']);
                var category = pins[item['category']] ?? embassy;
                if (item['latitude'] != "") {
                  markers.add(
                    Marker(
                        icon: category,
                        onTap: () {
                          showModalBottomSheet(
                            useRootNavigator: false,
                            context: context1,
                            builder: (BuildContext context) {
                              return MapSheet(service);
                            },
                          );
                        },
                        markerId: MarkerId(item['uid']),
                        position: LatLng(double.parse(item['latitude']),
                            double.parse(item['longitude']))),
                  );
                }
              }

              return GoogleMap(
                myLocationButtonEnabled: true,
                myLocationEnabled: true,
                initialCameraPosition: CameraPosition(
                    target: widget.position != null
                        ? LatLng(widget.position!.latitude,
                            widget.position!.longitude)
                        : const LatLng(36.62257816899407, 127.91520089316795),
                    zoom: widget.position != null ? 13 : mainZoom),
                // Markers to be pointed
                markers: markers,
                onMapCreated: (controller) {
                  _googleMapController = controller;
                },
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
