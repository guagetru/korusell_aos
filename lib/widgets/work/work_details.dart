import 'package:flutter/material.dart';
import 'package:work_app/model/adv.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import './work_details_view.dart';

class WorkDetail extends StatelessWidget {
  final Adv adv;

  const WorkDetail({Key? key, required this.adv}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: 'Работа'),
      body: SingleChildScrollView(
        child: Column(children: [
          WorkDetailsView(adv: adv),
          const SizedBox(height: 60),
        ]),
      ),
    );
  }
}
