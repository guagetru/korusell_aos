import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:work_app/model/adv.dart';
import 'package:work_app/utils/constants.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/work/work_list_view.dart';
import 'work_filter.dart';
import 'work_details.dart';

class WorkList extends StatefulWidget {
  const WorkList({Key? key}) : super(key: key);

  @override
  _WorkListState createState() => _WorkListState();
}

class _WorkListState extends State<WorkList> {
  String category = "all";
  String city = "Все Города";
  String categoryName = "Все";
  callback(newCategory, newCity) {
    setState(() {
      category = newCategory;
      city = newCity;
    });
  }

  @override
  Widget build(BuildContext context) {
    final allWorkallCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .orderBy('updatedAt', descending: true)
        .snapshots();
    final allWorksomeCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .where("city", isEqualTo: city)
        .orderBy('updatedAt', descending: true)
        .snapshots();
    final someWorkallCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .where("subcategory", isEqualTo: category)
        .orderBy('updatedAt', descending: true)
        .snapshots();
    final someWorksomeCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .where("subcategory", isEqualTo: category)
        .where("city", isEqualTo: city)
        .orderBy('updatedAt', descending: true)
        .snapshots();
    final allWorkotherCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .orderBy('city')
        // .where("city", whereNotIn: DBcities)
        .orderBy('updatedAt', descending: true)
        .snapshots();
    final someWorkotherCity = FirebaseFirestore.instance
        .collection('adv')
        .where('isActive', isEqualTo: '1')
        .where("subcategory", isEqualTo: category)
        .orderBy('city')
        // .where("city", whereNotIn: DBcities)
        .orderBy('updatedAt', descending: true)
        .snapshots();

    var reference = someWorksomeCity;
    if (category == "all") {
      if (city == "Другой Город") {
        reference = allWorkotherCity;
      } else if (city == "Все Города") {
        reference = allWorkallCity;
      } else {
        reference = allWorksomeCity;
      }
    } else if (category != "all") {
      if (city == "Другой Город") {
        reference = someWorkotherCity;
      } else if (city == "Все Города") {
        reference = someWorkallCity;
      }
    }

    return Scaffold(
      // backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: MyAppBar(
        label: 'Работа',
        actions: [
          IconButton(
            color: Colors.black,
            icon: const Icon(CupertinoIcons.line_horizontal_3_decrease),
            onPressed: () {
              showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return WorkFilter(category, city, callback);
                  });
            },
          ),
        ],
      ),
      body: StreamBuilder(
          stream: reference,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              // return const Text('Пофиксить placeholder');
              return ListView.separated(
                itemCount: 6,
                itemBuilder: (BuildContext context, int index) {
                  return SizedBox(
                      height: MediaQuery.of(context).size.height * 0.15,
                      child: const InkWell());
                },
                separatorBuilder: (BuildContext context, int index) {
                  return const Divider(
                      // color: Colors.black,
                      );
                },
              );
            } else {
              var docs = snapshot.data!.docs
                ..removeWhere((element) => element['category'] != 'work');
              if (city == 'Другой Город') {
                docs = snapshot.data!.docs
                  ..removeWhere((element) =>
                      element['category'] != 'work' ||
                      element['city'] == 'Ансан' ||
                      element['city'] == 'Хвасонг' ||
                      element['city'] == 'Инчхон' ||
                      element['city'] == 'Сеул' ||
                      element['city'] == 'Сувон' ||
                      element['city'] == 'Асан' ||
                      element['city'] == 'Чхонан' ||
                      element['city'] == 'Чхонджу' ||
                      element['city'] == 'Пхёнтхэк' ||
                      element['city'] == 'Сосан' ||
                      element['city'] == 'Дунпо')
                  ..shuffle();
              }
              return ListView.separated(
                itemCount: docs.length,
                itemBuilder: (BuildContext context, int index) {
                  final QueryDocumentSnapshot<Object?>? data = docs[index];
                  Adv adv = Adv(
                    id: data?.get('id'),
                    name: data?.get('name'),
                    category: data?.get('category'),
                    city: data?.get('city'),
                    phone: data?.get('phone'),
                    description: data?.get('description'),
                    age: data?.get('age'),
                    createdAt: data?.get('createdAt'),
                    gender: data?.get('gender'),
                    images: data?.get('images'),
                    isActive: data?.get('isActive'),
                    price: data?.get('price'),
                    shift: data?.get('shift'),
                    subcategory: data?.get('subcategory'),
                    uid: data?.get('uid'),
                    updatedAt: data?.get('updatedAt'),
                    visa: data?.get('visa'),
                  );
                  return WorkView(
                      adv: adv,
                      widget: WorkDetail(
                        adv: adv,
                      ));
                  // InkWell(
                  //   child: Container(
                  //     child: ListTile(
                  //       title: Row(
                  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //           children: [
                  //             const Text(
                  //               'Город:',
                  //               style: TextStyle(fontSize: 13),
                  //             ),
                  //             Text(
                  //               adv.city,
                  //               style: const TextStyle(
                  //                   fontSize: 13, fontWeight: FontWeight.bold),
                  //             ),
                  //             const SizedBox(
                  //               width: 30,
                  //             ),
                  //             Text(
                  //               formattedDate(adv.updatedAt),
                  //               style: const TextStyle(fontSize: 13),
                  //             ),
                  //           ]),

                  //       subtitle: Column(
                  //         mainAxisAlignment: MainAxisAlignment.start,
                  //         crossAxisAlignment: CrossAxisAlignment.start,
                  //         children: [
                  //           const SizedBox(
                  //             height: 5,
                  //           ),
                  //           Text(
                  //             adv.name,
                  //             maxLines: 1,
                  //             style: const TextStyle(
                  //               color: Colors.black,
                  //               fontSize: 17,
                  //               fontWeight: FontWeight.bold,
                  //             ),
                  //           ),
                  //           const SizedBox(
                  //             height: 5,
                  //           ),
                  //           Text(
                  //             adv.phone,
                  //             maxLines: 1,
                  //             style: Theme.of(context).textTheme.bodyText1,
                  //           ),
                  //           Padding(
                  //             padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  //             child: WorkStatusWidget(adv),
                  //           ),
                  //         ],
                  //       ),
                  //       // trailing: Icon(Icons.arrow_forward_ios),
                  //       onTap: () => {
                  //         Navigator.push(
                  //             context,
                  //             MaterialPageRoute(
                  //               builder: (context) => DetailPage(adv: adv),
                  //             )),
                  //       },
                  //     ),
                  //   ),
                  // );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return const Divider();
                },
              );
            }
          }),
    );
  }
}
