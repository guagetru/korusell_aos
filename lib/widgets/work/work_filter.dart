import 'package:flutter/material.dart';
import 'package:work_app/utils/constants.dart';

class WorkFilter extends StatefulWidget {
  final String category;
  final String city;
  final Function(String, String) callback;

  const WorkFilter(this.category, this.city, this.callback, {Key? key})
      : super(key: key);

  @override
  State<WorkFilter> createState() => _WorkFilterState();
}

class _WorkFilterState extends State<WorkFilter> {
  var myCity = "Город";
  var myCategory = "Категория";

  @override
  void initState() {
    super.initState();
    myCity = widget.city;
    myCategory = widget.category;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Text(
            'Добавить Фильтры',
            style: TextStyle(
              fontSize: 20,
              // color: Colors.white,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              const Text(
                "Категория:  ",
                // style: TextStyle(color: Colors.white),
              ),
              PopupMenuButton(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: myCategory == "all"
                      ? Text(
                          'Все',
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                          ),
                        )
                      : Text(
                          workIcons[myCategory]![1],
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                  // style: TextStyle(color: Colors.white),
                ),
                itemBuilder: (BuildContext context) {
                  return List.generate(categories.length, (index) {
                    return PopupMenuItem(
                      onTap: () {
                        setState(() {
                          myCategory = categories[index];
                        });
                      },
                      child: Text(categoriesNames[index]),
                    );
                  });
                },
              ),
            ],
          ),
          Row(
            children: [
              const Text(
                "Город:  ",
                // style: TextStyle(color: Colors.white),
              ),
              PopupMenuButton(
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      myCity,
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                    )),
                itemBuilder: (BuildContext context) {
                  return List.generate(cities.length, (index) {
                    return PopupMenuItem(
                      onTap: () {
                        setState(() {
                          myCity = cities[index];
                        });
                      },
                      child: Text(cities[index]),
                    );
                  });
                },
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.white, elevation: 1),
                  onPressed: () {
                    widget.callback(myCategory, myCity);
                    Navigator.pop(context);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        'Применить',
                        style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  )),
            ],
          ),
          const SizedBox(
            height: 60,
          )
        ],
      ),
    );
  }
}
