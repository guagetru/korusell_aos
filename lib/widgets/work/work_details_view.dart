import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:work_app/widgets/_elements/owner.dart';

import '../../model/adv.dart';
import '../../utils/constants.dart';
import '../../utils/functions.dart';
import '../_elements/buttons.dart';

class WorkDetailsView extends StatelessWidget {
  const WorkDetailsView({
    Key? key,
    required this.adv,
  }) : super(key: key);

  final Adv adv;

  @override
  Widget build(BuildContext context) {
    // final String avatarUrl = 'gs://korusell.appspot.com/avatars/' + adv.uid + '.jpg';
    if (adv.uid.length > 27) {}

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const Text(
                    'Добавлено:  ',
                    style: TextStyle(fontSize: 13, color: Colors.grey),
                  ),
                  Text(
                    formattedDate(adv.updatedAt),
                    style: const TextStyle(fontSize: 13, color: Colors.grey),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Text(
                adv.name,
                maxLines: 2,
                style:
                    const TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              Row(
                children: [
                  const Text('Город:  '),
                  Text(adv.city),
                ],
              ),
              const SizedBox(height: 10),
              Row(
                children: [
                  const Text('ЗП:  '),
                  Flexible(
                    child: Text(
                      adv.price,
                      maxLines: 2,
                      // overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          fontSize: 17, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              const Divider(),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        workIcons[adv.subcategory]![0],
                        style: const TextStyle(fontSize: 20),
                      ),
                      const SizedBox(height: 10),
                      const Text('Пол:'),
                      const SizedBox(height: 10),
                      const Text('Смена:'),
                      const SizedBox(height: 10),
                      const Text('Возраст:'),
                      const SizedBox(height: 10),
                      const Text('Визы:'),
                      const SizedBox(height: 5),
                    ],
                  ),
                  const SizedBox(width: 30),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 5),
                      Text(workIcons[adv.subcategory]![1]),
                      const SizedBox(height: 5),
                      Text(
                        adv.gender,
                        style: const TextStyle(fontSize: 20),
                      ), //#fix Мужчина и Женщина
                      const SizedBox(height: 5),
                      Text(
                        adv.shift,
                        style: const TextStyle(fontSize: 20),
                      ), //#fix Чуган Яган
                      const SizedBox(height: 5),
                      (int.parse(adv.age[0]) <= 20 &&
                              int.parse(adv.age[1]) >= 70)
                          ? const Text(
                              'Любой',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            )
                          : Text(
                              adv.age.join(" - ") + " лет",
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold),
                            ),
                      const SizedBox(height: 10),
                      Text(
                        adv.visa.join(", "),
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
              const Divider(),
              const Text(
                'Описание',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              Text(adv.description),
              const Divider(),
              const SizedBox(height: 20),
            ],
          ),
        ),
        Center(
          child: SubmitButton(
              text: adv.phone, onPressed: () => launch("tel://${adv.phone}")),
        ),
        const SizedBox(height: 20),
        if (adv.uid.length > 27) Owner(adv.uid.substring(0, 28)),
        // TextButton.icon(
        //   onPressed: () {
        //     showModalBottomSheet(
        //       useRootNavigator: false,
        //       context: context,
        //       builder: (BuildContext context) {
        //         var uid = adv.uid;
        //         if (adv.uid.length > 27) {
        //           uid = adv.uid.substring(0, 28);
        //         }
        //         return ContactView(uid, avatarUrl);
        //       },
        //     );
        //   },
        //   icon: ClipRRect(
        //       borderRadius: BorderRadius.circular(10),
        //       child: CustomImage(avatarUrl, 20, 20, true)),
        //   label: const Text("Контактное лицо"),
        // ),
      ],
    );
  }
}
