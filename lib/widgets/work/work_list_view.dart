import 'package:flutter/material.dart';

import '../../model/adv.dart';
import '../../utils/constants.dart';
import '../../utils/functions.dart';

class WorkView extends StatelessWidget {
  final Adv adv;
  final Widget widget;
  const WorkView({Key? key, required this.adv, required this.widget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: double.parse(adv.isActive),
      child: InkWell(
        child: ListTile(
          title:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            const Text(
              'Город:',
              style: TextStyle(fontSize: 13),
            ),
            Flexible(
              child: Text(
                adv.city,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style:
                    const TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(
              width: 30,
            ),
            Text(
              formattedDate(adv.updatedAt),
              style: const TextStyle(fontSize: 13),
            ),
          ]),

          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 5,
              ),
              Text(
                adv.name,
                maxLines: 1,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  const Text('ЗП: '),
                  Flexible(
                    child: Text(
                      adv.price,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: WorkStatusWidget(adv),
              ),
            ],
          ),
          // trailing: Icon(Icons.arrow_forward_ios),
          onTap: () => {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => widget)),
          },
        ),
      ),
    );
  }
}

class WorkStatusWidget extends StatelessWidget {
  // final QueryDocumentSnapshot<Object?>? data;
  final Adv adv;
  const WorkStatusWidget(this.adv, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          children: [
            Text(
              workIcons[adv.subcategory]![2],
              overflow: TextOverflow.ellipsis,
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              workIcons[adv.subcategory]![0],
              style: const TextStyle(
                color: Colors.black,
              ),
            ),
          ],
        ),
        Column(
          children: [
            const Text('Пол'),
            const SizedBox(
              height: 5,
            ),
            Text(
              adv.gender,
              style: const TextStyle(
                color: Colors.black,
              ),
            ),
          ],
        ),
        Column(
          children: [
            const Text('Смена'),
            const SizedBox(
              height: 5,
            ),
            Text(adv.shift,
                style: const TextStyle(
                  color: Colors.black,
                )),
          ],
        ),
        Column(
          children: [
            const Text(
              'Возраст',
            ),
            const SizedBox(
              height: 5,
            ),
            (int.parse(adv.age[0]) <= 20 && int.parse(adv.age[1]) >= 70)
                ? const Text(
                    'Любой',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  )
                : Text(
                    adv.age.join(" - ") + " лет",
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  ),
          ],
        ),
        Column(
          children: [
            const Text(
              'Виза',
            ),
            const SizedBox(
              height: 5,
            ),
            Text(adv.visa.join(", "),
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                )),
          ],
        ),
      ],
    );
  }
}
