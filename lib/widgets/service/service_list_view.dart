import 'package:flutter/material.dart';

import '../../model/service.dart';
import '../../utils/image_controller/custom_image.dart';

class ServiceListView extends StatelessWidget {
  final String imageUrl;
  final Service service;
  final Function onTap;

  const ServiceListView({
    Key? key,
    required this.imageUrl,
    required this.service,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: SizedBox(
        child: ListTile(
            title: Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  children: [
                    CustomImage(
                        imageUrl,
                        MediaQuery.of(context).size.height * 0.14,
                        MediaQuery.of(context).size.height * 0.14,
                        true),
                  ],
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  width: MediaQuery.of(context).size.width * 0.6,
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        service.name,
                        maxLines: 2,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.01,
                      ),
                      Text(
                        'Город:  ' + service.city,
                        style:
                            const TextStyle(fontSize: 12, color: Colors.grey),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.01,
                      ),
                      Text(
                        service.description,
                        style: const TextStyle(fontSize: 12),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                      ),
                    ],
                  ),
                )
              ],
            ),
            // trailing: Column(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     Icon(
            //       Icons.arrow_forward_ios,
            //       size: 20,
            //     ),
            //   ],
            // ),
            onTap: () => onTap()
            // {
            //   Navigator.push(
            //       context, MaterialPageRoute(builder: (context) => widget)),
            // },
            ),
      ),
    );
  }
}
