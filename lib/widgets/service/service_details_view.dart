import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:work_app/widgets/_elements/owner.dart';

import '../../model/service.dart';
import '../../utils/image_controller/custom_image.dart';
import '../_elements/buttons.dart';

class ServiceDetailsView extends StatefulWidget {
  const ServiceDetailsView({
    Key? key,
    required this.service,
  }) : super(key: key);

  final Service service;

  @override
  State<ServiceDetailsView> createState() => _ServiceDetailsViewState();
}

class _ServiceDetailsViewState extends State<ServiceDetailsView> {
  double currentIndexPage = 0.0;

  Widget carousel() {
    return Column(
      children: [
        Center(
          child: CarouselSlider.builder(
            itemCount: int.parse(widget.service.images),
            itemBuilder:
                (BuildContext context, int itemIndex, int pageViewIndex) {
              return CustomImage(
                  'gs://korusell.appspot.com/images/' +
                      widget.service.uid +
                      itemIndex.toString() +
                      '.jpg',
                  MediaQuery.of(context).size.height * 0.42,
                  double.infinity / 2,
                  true);
            },
            options: CarouselOptions(
              onPageChanged: ((index, reason) => setState(() {
                    currentIndexPage = index.ceilToDouble();
                    // print(currentIndexPage);
                  })),
              enableInfiniteScroll: false,
              autoPlay: false,
              enlargeCenterPage: true,
              viewportFraction: 1,
              aspectRatio: 1,
              initialPage: 0,
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Center(
          child: DotsIndicator(
            decorator: const DotsDecorator(
              activeColor: Colors.blue,
              // activeSize: Size.square(10),
              size: Size.square(7),
              spacing: EdgeInsets.symmetric(horizontal: 4),
            ),
            mainAxisSize: MainAxisSize.min,
            dotsCount: int.parse(widget.service.images),
            position: currentIndexPage,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        (int.parse(widget.service.images) > 1)
            ? carousel()
            : CustomImage(
                'gs://korusell.appspot.com/images/' +
                    widget.service.uid +
                    '0' +
                    '.jpg',
                MediaQuery.of(context).size.height * 0.42,
                double.infinity / 2,
                true),
        Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (!widget.service.uid.contains('aaaaaaaaaaaaaaaaaaaaaaaa'))
                Owner(widget.service.uid),
              if (widget.service.address != "")
                Row(
                  children: [
                    const Text('Адрес:  '),
                    Flexible(
                      child: Text(
                        widget.service.address,
                        softWrap: false,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    ),
                    IconButton(
                        onPressed: () {
                          Clipboard.setData(
                              ClipboardData(text: widget.service.address));
                          final snackbar = SnackBar(
                            content: Text(widget.service.address),
                            action: SnackBarAction(
                              label: 'Адрес скопирован',
                              onPressed: () {
                                // Some code to undo the change.
                              },
                            ),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackbar);
                        },
                        icon: const Icon(
                          Icons.copy,
                          size: 15,
                          color: Colors.blue,
                        )),
                  ],
                ),
              const SizedBox(height: 10),
              Text(
                widget.service.name,
                maxLines: 2,
                style:
                    const TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              Row(
                children: [const Text('Город:  '), Text(widget.service.city)],
              ),
              const SizedBox(height: 10),
              const Divider(
                height: 10,
                thickness: 1,
              ),
              const SizedBox(height: 10),
              const Text(
                'Описание',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 5),
              Text(widget.service.description),
              const SizedBox(height: 10),
              const Divider(
                height: 10,
                thickness: 1,
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
        if (widget.service.phone != "")
          Center(
            child: SubmitButton(
                text: widget.service.phone,
                onPressed: () => launch("tel://${widget.service.phone}")),
          ),
        const SizedBox(height: 20),
        SocialWidget(widget.service),
      ],
    );
  }
}

class SocialWidget extends StatelessWidget {
  final Service service;
  const SocialWidget(this.service, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // print(service.social.toString());
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if (service.social[0] != "" &&
            service.social[0] != "https://www.facebook.com/")
          _SocialButton('facebook', service),
        if (service.social[1] != "" &&
            service.social[1] != "https://www.instagram.com/")
          _SocialButton('instagram', service),
        if (service.social[2] != "" && service.social[2] != "https://t.me/")
          _SocialButton('telegram', service),
        if (service.social[3] != "" &&
            service.social[3] != "https://www.youtube.com/")
          _SocialButton('youtube', service),
        if (service.social[4] != "" && service.social[4] != "https://")
          _SocialButton('webpage', service),
      ],
    );
  }
}

class _SocialButton extends StatelessWidget {
  final String name;
  final Service service;
  const _SocialButton(this.name, this.service);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      padding: const EdgeInsets.symmetric(horizontal: 13),
      icon: Image.asset('assets/images/social/' + name + '.png'),
      iconSize: 35,
      onPressed: () {
        if (name == "facebook") launch(service.social[0]);
        if (name == "instagram") launch(service.social[1]);
        if (name == "telegram") launch(service.social[2]);
        if (name == "youtube") launch(service.social[3]);
        if (name == "webpage") launch(service.social[4]);
      },
    );
  }
}
