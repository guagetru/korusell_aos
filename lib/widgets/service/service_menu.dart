import 'package:flutter/material.dart';
import 'package:work_app/utils/constants.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/service/service_list.dart';
import 'package:package_info_plus/package_info_plus.dart';

class ServiceMenu extends StatelessWidget {
  const ServiceMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        // actions: [],
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                padding: const EdgeInsets.all(5),
                height: 40,
                width: 160,
                child: InkWell(
                    onLongPress: () => {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const VersionView()))
                        },
                    child: Image.asset('assets/images/title.png'))),
            // const Text(
            //   'Услуги',
            //   style: TextStyle(
            //     color: Colors.black,
            //     fontWeight: FontWeight.bold,
            //     fontSize: 20,
            //   ),
            // ),
          ],
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: ListView.separated(
          // padding: EdgeInsets.symmetric(horizontal: 20),
          itemCount: servicesMenu.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ServiceList(servicesMenuDB[index])),
                );
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(children: [
                  Text(servicesMenu[index][0],
                      style: const TextStyle(fontSize: 20)),
                  Text(servicesMenu[index][1],
                      style: const TextStyle(fontSize: 13)),
                ]),
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return const Divider();
          },
        ),
      ),
    );
  }
}

class VersionView extends StatefulWidget {
  const VersionView({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _VersionViewState createState() => _VersionViewState();
}

class _VersionViewState extends State<VersionView> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
    buildSignature: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  Widget _infoTile(String title, String subtitle) {
    return ListTile(
      title: Text(title),
      subtitle: Text(subtitle.isEmpty ? 'Not set' : subtitle),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: 'Korusell'),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          const SizedBox(
            height: 100,
          ),

          _infoTile('App name', _packageInfo.appName),
          // _infoTile('Package name', _packageInfo.packageName),
          _infoTile('App version', _packageInfo.version),
          _infoTile('Build number', _packageInfo.buildNumber),
          // _infoTile('Build signature', _packageInfo.buildSignature),
        ],
      ),
    );
  }
}
