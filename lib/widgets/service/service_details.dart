import 'package:flutter/material.dart';
import 'package:work_app/model/service.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import './service_details_view.dart';

class ServiceDetails extends StatelessWidget {
  final Service service;

  const ServiceDetails(this.service, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(label: service.name),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ServiceDetailsView(
              service: service,
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),
    );
  }
}
