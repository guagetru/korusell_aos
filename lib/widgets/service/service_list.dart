import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';
import 'package:work_app/utils/constants.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';
import 'package:work_app/widgets/service/service_list_view.dart';
import './service_details.dart';
import '../../model/service.dart';

class ServiceList extends StatefulWidget {
  final String category;

  const ServiceList(this.category, {Key? key}) : super(key: key);

  @override
  _ServiceListState createState() => _ServiceListState();
}

class _ServiceListState extends State<ServiceList> {
  String category = "all";
  String city = "Все Города";

  get onChanged => null;
  // callback(newCategory, newCity) {
  //   setState(() {
  //     category = newCategory;
  //     city = newCity;
  //   });
  // }

  @override
  void initState() {
    super.initState();
    category = widget.category;
  }

  @override
  Widget build(BuildContext context) {
    var allCity = FirebaseFirestore.instance
        .collection('services')
        .where('category', isEqualTo: category)
        .snapshots();
    var someCity = FirebaseFirestore.instance
        .collection('services')
        .where('category', isEqualTo: category)
        .where('city', isEqualTo: city)
        .snapshots();
    var otherCity = FirebaseFirestore.instance
        .collection('services')
        .where('category', isEqualTo: category)
        // .where('city', whereNotIn: DBcities)
        .snapshots();

    var reference = someCity;
    if (city == 'Другой Город') {
      reference = otherCity;
    } else if (city == 'Все Города') {
      reference = allCity;
    }

    return Scaffold(
      // backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: MyAppBar(
        label: servicesNames[category]!,
        actions: [
          DropdownButton(
            value: city,
            underline: const Text(''),
            items: cities.map((String items) {
              return DropdownMenuItem(
                child: Text(
                  items,
                  style: const TextStyle(fontSize: 14),
                ),
                value: items,
              );
            }).toList(),
            onChanged: (String? newValue) {
              setState(() {
                city = newValue!;
              });
            },
          ),
        ],
      ),
      body: StreamBuilder(
        stream: reference,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            // return const Center(child: Text('Placeholder'));
            return ListView.separated(
              itemCount: 6,
              itemBuilder: (BuildContext context, int index) {
                return SizedBox(
                    height: MediaQuery.of(context).size.height * 0.15,
                    child: const InkWell());
              },
              separatorBuilder: (BuildContext context, int index) {
                return const Divider(
                    // color: Colors.black,
                    );
              },
            );
          } else {
            var docs = snapshot.data!.docs..shuffle();
            if (city == 'Другой Город') {
              docs = snapshot.data!.docs
                ..removeWhere((element) =>
                    element['city'] == 'Ансан' ||
                    element['city'] == 'Хвасонг' ||
                    element['city'] == 'Инчхон' ||
                    element['city'] == 'Сеул' ||
                    element['city'] == 'Сувон' ||
                    element['city'] == 'Асан' ||
                    element['city'] == 'Чхонан' ||
                    element['city'] == 'Чхонджу' ||
                    element['city'] == 'Пхёнтхэк' ||
                    element['city'] == 'Сосан' ||
                    element['city'] == 'Дунпо')
                ..shuffle();
            }
            return ListView.separated(
              itemCount: docs.length,
              itemBuilder: (BuildContext context, int index) {
                final QueryDocumentSnapshot<Object?>? data = docs[index];
                Service service = Service(
                  // id: data?.get('id'),
                  address: data?.get('address'),
                  category: data?.get('category'),
                  city: data?.get('city'),
                  description: data?.get('description'),
                  images: data?.get('images'),
                  latitude: data?.get('latitude'),
                  longitude: data?.get('longitude'),
                  name: data?.get('name'),
                  phone: data?.get('phone'),
                  social: data?.get('social'),
                  uid: data?.get('uid'),
                );
                String imageUrl = 'gs://korusell.appspot.com/images/' +
                    service.uid +
                    '0' +
                    '.jpg';
                return ServiceListView(
                  imageUrl: imageUrl, service: service,
                  onTap: () => navigate(service),
                  // widget: ServiceDetails(service),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return const Divider();
              },
            );
          }
        },
      ),
    );
  }

  void navigate(Service service) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ServiceDetails(service)));
  }
}
