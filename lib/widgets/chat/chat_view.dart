import 'package:flutter/material.dart';
import 'package:work_app/model/message.dart';
import 'package:work_app/widgets/chat/single_message.dart';

class ChatView extends StatefulWidget {
  const ChatView({Key? key}) : super(key: key);

  @override
  State<ChatView> createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> {
  List<Message> messages = [
    Message(
        uid: '1sldGFghl9hcy2l0rtI7u9UhiLT2',
        question:
            'Ребята, всем привет, подскажите пожалуйста, как записаться в имигрэшку, если есть возможность, то поподробнее, очень буду благодарен)',
        answers: {'dwq': 'qdw', 'asddsa': 'saddsa'},
        solved: true,
        imageUrl: 'imageUrl',
        createdDate: '22.03.17',
        category: '🛂 Имиграция'),
    Message(
        uid: 'o1ajACtmM6cF2Ho2aEpqcIRZIAp1',
        question:
            'Доброе утро. Подскажите, дают ли студенческую визу на магистратуру тем у кого был G1, если университет аккредитован и ты не нарушал ничего, когда был в Корее?',
        answers: {'dwq': 'qdw'},
        solved: false,
        imageUrl: 'imageUrl',
        createdDate: '22.03.17',
        category: '🎫 Виза'),
    Message(
        uid: 'rpM7GN3F49eEkytiAsyOWgC77VC3',
        question:
            'Здравствуйте всем! Такой вопрос, можно ли в Корее работать с визой F1? Куда берут, куда нет?',
        answers: {'dwq': 'qdw'},
        solved: true,
        imageUrl: 'imageUrl',
        createdDate: '22.03.17',
        category: '🛠 Работа'),
    Message(
        uid: '5dzWSAEBuOS95fGm35WqnwTcL3J2',
        question:
            'Здравствуйте. Подскажите пожалуйста. Как платить налоги если работаешь фрилансером? Спасибо.',
        answers: {'dwq': 'qdw'},
        solved: false,
        imageUrl: 'imageUrl',
        createdDate: '22.03.17',
        category: '💰 Налоги'),
    Message(
        uid: 'ytv8oNkq9WUPvuCNx4SaeV1eKeJ3',
        question:
            'Здравствуйте. Кто-то знает номер русскоязычного сотрудника главного иммиграционного офиса в Корее?',
        answers: {'dwq': 'qdw'},
        solved: true,
        imageUrl: 'imageUrl',
        createdDate: '22.03.17',
        category: '🛂 Имиграция'),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: messages.length,
      itemBuilder: (context, index) {
        return SingleMessage(messages[index]);
      },
      separatorBuilder: (BuildContext context, int index) {
        return const Divider();
      },
    );
  }
}
