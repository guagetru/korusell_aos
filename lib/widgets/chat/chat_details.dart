import 'package:flutter/material.dart';
import 'package:work_app/model/message.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';

import 'answers.dart';

class ChatDetails extends StatefulWidget {
  final Message message;
  const ChatDetails(this.message, {Key? key}) : super(key: key);

  @override
  State<ChatDetails> createState() => _ChatDetailsState();
}

class _ChatDetailsState extends State<ChatDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar(label: 'Ответы'), body: Answers(widget.message));
  }
}
