import 'package:flutter/material.dart';
import 'package:work_app/widgets/_elements/app_bar.dart';

import 'chat_view.dart';

class Chat extends StatelessWidget {
  const Chat({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: MyAppBar(label: 'Беседка'), body: const ChatView());
  }
}
