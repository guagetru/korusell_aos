import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:work_app/widgets/_elements/owner.dart';
import 'package:work_app/widgets/chat/chat_details.dart';

import '../../model/message.dart';

class SingleMessage extends StatelessWidget {
  final Message message;

  const SingleMessage(this.message, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: InkWell(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ChatDetails(message)));
        },
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Owner(message.uid),
                const Text(
                  '2022.03.18',
                  style: TextStyle(color: Colors.grey),
                ),
              ],
            ),
            Text(
              message.question,
              maxLines: 6,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    message.category,
                    style: const TextStyle(color: Colors.grey),
                  ),
                  message.solved
                      ? const Text(
                          '✅ Решенный',
                          style: TextStyle(color: Colors.grey),
                        )
                      : const Text(
                          ' Не решенный',
                          style: TextStyle(color: Colors.grey),
                        ),
                  Row(
                    children: [
                      const Icon(
                        CupertinoIcons.chat_bubble,
                        size: 15,
                        color: Colors.grey,
                      ),
                      const SizedBox(
                        width: 3,
                      ),
                      Text(
                        message.answers.length.toString(),
                        style: const TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
