import 'package:flutter/material.dart';
import 'package:work_app/model/message.dart';

class Answers extends StatefulWidget {
  final Message message;
  const Answers(this.message, {Key? key}) : super(key: key);

  @override
  State<Answers> createState() => _AnswersState();
}

class _AnswersState extends State<Answers> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(widget.message.question),
        Text(widget.message.category),
        Text(widget.message.createdDate),
      ],
    );
  }
}
