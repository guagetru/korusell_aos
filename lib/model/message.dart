class Message {
  final String uid;
  final String question;
  final Map<String, String> answers;
  final bool solved;
  final String createdDate;
  final String category;
  final String imageUrl;

  Message.fromJson(Map<String, Object?> json)
      : this(
          uid: json['uid']! as String,
          question: json['question']! as String,
          answers: json['answers']! as Map<String, String>,
          imageUrl: json['imageUrl'] != null ? json['imageUrl'] as String : '',
          solved: json['solved']! as bool,
          category: json['category'].toString(),
          createdDate: json['created_date'].toString(),
        );

  Map<String, Object?> toJson() {
    return {
      'question': question,
      'answers': answers,
      'uid': uid,
      'imageUrl': imageUrl,
      'solved': solved,
      'created_date': createdDate,
      'category': category,
    };
  }

  Message({
    required this.uid,
    required this.question,
    required this.answers,
    required this.solved,
    required this.imageUrl,
    required this.createdDate,
    required this.category,
  });
}
