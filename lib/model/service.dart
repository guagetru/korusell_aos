class Service {
  Service({
    required this.address,
    required this.category,
    required this.city,
    required this.description,
    // required this.id,
    required this.images,
    required this.latitude,
    required this.longitude,
    required this.name,
    required this.phone,
    required this.social,
    required this.uid,
  });

  Service.fromJson(Map<String, Object?> json)
      : this(
          name: json['name']! as String,
          category: json['category']! as String,
          address: json['address']! as String,
          city: json['city']! as String,
          description: json['description']! as String,
          images: json['images']! as String,
          latitude: json['latitude']! as String,
          longitude: json['longitude']! as String,
          phone: json['phone']! as String,
          social: json['social']! as List<dynamic>,
          uid: json['uid']! as String,
          // id: json['id']! as String,
        );

  final String address;
  final String category;
  final String city;
  final String description;
  // final String id;
  final String images;
  final String latitude;
  final String longitude;
  final String name;
  final String phone;
  final List<dynamic> social;
  final String uid;

  Map<String, Object?> toJson() {
    return {
      'name': name,
      'address': address,
      'category': category,
      'city': city,
      'description': description,
      'images': images,
      'latitude': latitude,
      'longitude': longitude,
      'phone': phone,
      'social': social,
      'uid': uid,
      // 'id': id,
    };
  }
}
