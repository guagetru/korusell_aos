class User {
  final String uid;
  final String email;
  final String name;
  final String phone;
  final String createdDate;
  final String lastLogin;
  final String imageUrl;

  User.fromJson(Map<String, Object?> json)
      : this(
          uid: json['uid']! as String,
          email: json['email']! as String,
          name: json['name'] != null ? json['name']! as String : '',
          imageUrl: json['imageUrl'] != null ? json['imageUrl'] as String : '',
          phone: json['phone']! as String,
          lastLogin: json['last_login'].toString(),
          createdDate: json['created_date'].toString(),
        );

  Map<String, Object?> toJson() {
    return {
      'name': name,
      'phone': phone,
      'uid': uid,
      'imageUrl': imageUrl,
      'last_login': lastLogin,
      'created_date': createdDate,
      'email': email,
    };
  }

  User({
    required this.uid,
    required this.name,
    required this.phone,
    required this.email,
    required this.imageUrl,
    required this.createdDate,
    required this.lastLogin,
  });
}
