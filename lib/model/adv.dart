class Adv {
  final List<dynamic> age;
  final String category;
  final String city;
  final String createdAt;
  final String description;
  final String gender;
  final String id;
  final String images;
  final String isActive;
  final String name;
  final String phone;
  final String price;
  final String shift;
  final String subcategory;
  final String uid;
  final String updatedAt;
  final List<dynamic> visa;

  Adv.fromJson(Map<String, Object?> json)
      : this(
          name: json['name']! as String,
          category: json['category']! as String,
          age: json['age']! as List<dynamic>,
          city: json['city']! as String,
          description: json['description']! as String,
          images: json['images']! as String,
          gender: json['gender']! as String,
          shift: json['shift']! as String,
          phone: json['phone']! as String,
          visa: json['visa']! as List<dynamic>,
          uid: json['uid']! as String,
          updatedAt: json['updatedAt']! as String,
          subcategory: json['subcategory']! as String,
          price: json['price']! as String,
          isActive: json['isActive']! as String,
          id: json['id']! as String,
          createdAt: json['createdAt']! as String,
        );

  Map<String, Object?> toJson() {
    return {
      'name': name,
      'age': age,
      'category': category,
      'city': city,
      'description': description,
      'images': images,
      'gender': gender,
      'shift': shift,
      'phone': phone,
      'visa': visa,
      'uid': uid,
      'updatedAt': updatedAt,
      'subcategory': subcategory,
      'price': price,
      'isActive': isActive,
      'id': id,
      'createdAt': createdAt,
    };
  }

  Adv({
    required this.age,
    required this.category,
    required this.city,
    required this.createdAt,
    required this.description,
    required this.gender,
    required this.id,
    required this.images,
    required this.isActive,
    required this.name,
    required this.phone,
    required this.price,
    required this.shift,
    required this.subcategory,
    required this.uid,
    required this.updatedAt,
    required this.visa,
  });
}
